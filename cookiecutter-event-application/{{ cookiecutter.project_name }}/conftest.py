import json
import os
from pathlib import Path

import pytest
from eventutilities_python.flink import EventDataStreamFactory, get_flink_env
from eventutilities_python.stream import EventStreamDescriptor
from pyflink.datastream import RuntimeExecutionMode

pwd = Path(__file__).parent.resolve()
tests_path = f"{pwd}/tests"

schema_uris = [f"file://{tests_path}/event-schemas/repo0"]
stream_config_uri = f"file://{tests_path}/event_stream_config.json"

print(schema_uris, stream_config_uri)


@pytest.fixture(scope="session")
def fixture_events_files():
    return [os.path.join(tests_path, "events.json")]


@pytest.fixture(scope="session")
def fixture_sink_path(tmp_path_factory):
    return str(tmp_path_factory.mktemp("output"))


@pytest.fixture(scope="session")
def fixture_error_path(tmp_path_factory):
    return str(tmp_path_factory.mktemp("error"))


@pytest.fixture
def fixture_stream_manager_default_config_files():
    """
    Returns a config file path for use with all python tests.
    """
    return [os.path.join(tests_path, "config.test.defaults.yaml")]


@pytest.fixture
def fixture_stream_manager_defaults(
    fixture_events_files, fixture_sink_path, fixture_error_path
):
    """
    Returns a dict of jsonargparse defaults for stream_manager to use
    in tests.  Most importantly the input and output uri paths
    are generated dynamically in tmp_path.
    """
    return {
        "stream_manager.source.options": {"uris": fixture_events_files},
        "stream_manager.sink.options": {"uri": fixture_sink_path},
        # TODO: when error sink config is fixed in eventutilities_python
        "stream_manager.error_sink.options": {"uri": fixture_error_path},
    }
