import os

import pytest
from eventutilities_python.testing.utils import read_records_from_flink_row_file_sink
from {{cookiecutter.project_name}}.app import main, get_config

# FIXME: I randomly bumped into  https://github.com/apache/flink/pull/17239. Local flink issue?
os.environ["_python_worker_execution_mode"] = "process"


def test_enrich(
    fixture_stream_manager_default_config_files,
    fixture_stream_manager_defaults,
):
    config = get_config(
        argv=[],  # "--config", fixture_stream_manager_default_config_files[0]],
        default_config_files=fixture_stream_manager_default_config_files,
        defaults=fixture_stream_manager_defaults,
    )

    # Run the main function
    main(config)

    # Read successful and errored events from the configured test file sinks.
    enriched_events = read_records_from_flink_row_file_sink(config.stream_manager.sink)
    for event in enriched_events:
        assert (event["test"] == "new_value")
