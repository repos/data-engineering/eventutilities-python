import logging
from typing import Optional, List, Dict, Any
from jsonargparse import ArgumentParser, Namespace
import requests
from eventutilities_python.stream import http_process_function
from eventutilities_python.stream import stream_manager, load_config


arg_parser = ArgumentParser(
    prog="{{cookiecutter.project_name}}",
    description="{{cookiecutter.description}}",
)

arg_parser_defaults = {
    "stream_manager.job_name": "{{cookiecutter.project_name}}",
}

{% if cookiecutter.uses_http != 'N' %}
# Since we will be using @http_process_function to decorate
# our http enrich process function, add params for it.
arg_parser.add_function_arguments(
    http_process_function,
    nested_key="http_session",
)
{% endif %}

def get_config(
    argv: List[str] = None,
    defaults: Optional[Dict[Any, Any]] = None,
    default_config_files: Optional[List[str]] = None,
) -> Namespace:
    defaults = defaults or {}

    return load_config(
        argv=argv,
        parser=arg_parser,
        defaults={**arg_parser_defaults, **defaults},
        default_config_files=default_config_files,
    )

def main(config: Namespace = None) -> None:
    config = config or load_config(parser=arg_parser, defaults=arg_parser_defaults)

    {% if cookiecutter.uses_http != 'N' %}
    @http_process_function(**config.http_session)
    def enrich(event: dict, http_session: requests.Session) -> dict:
    {% else %}
    def enrich(event: dict) -> dict:
    {% endif %}
        event["test"] = "new_value"
        return event

    with stream_manager.from_config(config) as stream:
        # Filter out canary events
        stream.filter(lambda e: e["meta"]["domain"] != "canary")
        # Process function needs a partition key.
        # This should be changed to something sensible for the stream you're using.
        stream.partition_by(lambda e: e["test"])
        stream.process(enrich)
        stream.execute()


if __name__ == "__main__":
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(process)s] (%(threadName)-10s) %(message)s",
    )
    main()
