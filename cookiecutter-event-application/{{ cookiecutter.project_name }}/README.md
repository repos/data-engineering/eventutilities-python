# {{ cookiecutter.project_name }}

Install the project `requirements.txt` and `requirements-test.txt` in a Python >= 3.9 virtual environment.
Alternatively, you can use Docker to test and run the application.

# Getting started

To create a new app, first configure source and sink streams in the `config.yaml` template.

While developing a new application it can be useful to work with a
local schema repository and EventStreamConfig configuration file.

The latter is particularly useful when working with streams that have
not yet been declared globally in Wikimedia's EventStreamConfig service.

## Stream Configuration

`event-schemas` contains a copy of Wikimedia's primary schema repo as a git `submodule`. 

A stream configuration is a json file that declares a list of stream names and their associated event schema title.
```json
{
  "mediawiki.page_change.v1": {
    "schema_title": "mediawiki/page/change"
  },
  "event_platform_app.sink": {
    "schema_title": "mediawiki/page/change"
  }
}
```
Depending on the stream's underlying representation, other properties may apply (e.g. 
a list of Kafka topics composing the stream). More information on the configuration
format can be found at https://wikitech.wikimedia.org/wiki/Event_Platform/Stream_Configuration.


## Run the application

Once configured, run the application with `python {{ cookiecutter.project_name }}/app.py --config config.yaml`

Or via docker by building a `development` variant of the application image:

```bash
DOCKER_BUILDKIT=1 docker build --target development -t {{ cookiecutter.project_name }}-devel -f .pipeline/blubber.yaml .
docker run {{ cookiecutter.project_name}}-devel
```

## Test the application

A test suite can be executed via `pytest`, or in docker by building a `test` variant of the application image:
```bash
DOCKER_BUILDKIT=1 docker build --target test -t {{ cookiecutter.project_name }}-test -f .pipeline/blubber.yaml .
docker run {{ cookiecutter.project_name}}-test
```

# CI and Docker images

Even Platform applications are dockerized and deployed on [k8s](https://wikitech.wikimedia.org/wiki/Kubernetes)
via [deployment-charts](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/%2B/refs/heads/master/).

To facilitate packaging, this application comes bundled with:
- a blubber file under `.pipeline/blubber.yaml` that declares `test`, `development` and `production` image variants.
- a `.gitlab-ci.yml` that integrates with Wikimedia's [Deployment Pipeline](https://wikitech.wikimedia.org/wiki/Deployment_pipeline) and can push images to our internal registry.
