FROM docker-registry.wikimedia.org/openjdk-11-jdk:11.0.18-s0-20230604 AS flink-image
USER 0
ENV HOME="/root"
ENV DEBIAN_FRONTEND="noninteractive"
RUN apt-get update && apt-get install -y "python3-pip" "python-is-python3" "python3-setuptools" "curl" "make" "git" "bash"
RUN python3 "-m" "pip" "install" "-U" "wheel" "tox"
