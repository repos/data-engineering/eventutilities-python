import dataclasses
import json
import os
from pathlib import Path

import yaml
import pytest
from pyflink.datastream import RuntimeExecutionMode

from eventutilities_python.flink import EventDataStreamFactory, get_flink_env
from eventutilities_python.stream.descriptor import (
    EventStreamDescriptor,
    SourceDescriptor,
    SinkDescriptor,
)
from eventutilities_python.stream.manager import stream_manager

# FIXME: I randomly bumped into  https://github.com/apache/flink/pull/17239. Local flink issue?
os.environ["_python_worker_execution_mode"] = "process"

pwd = Path(__file__).parent.resolve()
fixtures_path = f"{pwd}/tests"

schema_uris = f"file://{fixtures_path}/event-schemas/repo0"
stream_config_uri = f"file://{fixtures_path}/event_stream_config.json"


@pytest.fixture(scope="session")
def test_event_example_files():
    return [os.path.join(fixtures_path, "test.event.json")]


@pytest.fixture(scope="session")
def test_event_example_data(inp=f"{fixtures_path}/test.event.json"):
    with open(inp, "r") as fp:
        data = fp.readlines()

    return [json.loads(d) for d in data]


# TODO: rename these, they are EventStreamDescriptor fixtures.
@pytest.fixture(scope="session")
def test_event_example():
    return EventStreamDescriptor(name="test.event.example", version="1.1.0")

@pytest.fixture(scope="session")
def test_event_example_keyed():
    return EventStreamDescriptor(name="test.event.keyed.example", version="1.1.0")

@pytest.fixture(scope="session")
def test_event_enriched():
    return EventStreamDescriptor(name="test.event.enriched", version="1.1.0")


@pytest.fixture(scope="session")
def test_event_error():
    return EventStreamDescriptor(name="enrichment.error", version="2.1.0")


@pytest.fixture(scope="session")
def flink_env():
    env = get_flink_env()
    env.set_runtime_mode(RuntimeExecutionMode.BATCH)
    # set parallelism for tests to so we might catch errors
    # from running more than one taskmanager.
    env.set_parallelism(2)
    yield env
    env.close()


@pytest.fixture(scope="session")
def fixture_schema_uris():
    return schema_uris


@pytest.fixture(scope="session")
def fixture_stream_config_uri():
    return stream_config_uri


@pytest.fixture
def fixture_eventutilities_environ(monkeypatch, fixture_schema_uris, fixture_stream_config_uri):
    monkeypatch.setenv("SCHEMA_URIS", fixture_schema_uris)
    monkeypatch.setenv("STREAM_CONFIG_URI", fixture_stream_config_uri)


@pytest.fixture(scope="session")
def flink_datastream_factory(fixture_schema_uris, fixture_stream_config_uri):
    factory = EventDataStreamFactory.of(
        fixture_schema_uris.split(","), fixture_stream_config_uri, None
    )
    return factory


@pytest.fixture
def source_descriptor_file_test_event_example(test_event_example, test_event_example_files):
    return SourceDescriptor(
        connector="file",
        stream=str(test_event_example),
        options={"uris": test_event_example_files},
    )


@pytest.fixture
def sink_descriptor_file_test_event_enriched(tmp_path, test_event_enriched):
    main_output_path = tmp_path / "main_output"
    return SinkDescriptor(
        connector="file",
        stream=str(test_event_enriched),
        options={"uri": f"file://{str(main_output_path)}"},
    )


@pytest.fixture
def fixture_stream_manager_kwargs(
    fixture_schema_uris,
    fixture_stream_config_uri,
    source_descriptor_file_test_event_example,
    sink_descriptor_file_test_event_enriched,
):
    return {
        "job_name": "test_stream_manager_config",
        "schema_uris": fixture_schema_uris,
        "stream_config_uri": fixture_stream_config_uri,
        "source": dataclasses.asdict(source_descriptor_file_test_event_example),
        "sink": dataclasses.asdict(sink_descriptor_file_test_event_enriched),
    }


@pytest.fixture
def fixture_stream_manager_argv(tmp_path, fixture_stream_manager_kwargs):
    return [
        "--stream_manager.job_name",
        fixture_stream_manager_kwargs["job_name"],
        "--stream_manager.schema_uris",
        fixture_stream_manager_kwargs["schema_uris"],
        "--stream_manager.stream_config_uri",
        fixture_stream_manager_kwargs["stream_config_uri"],
        # convert a SourceDescriptor to json for arg parser, which expects all args to be strings.
        "--stream_manager.source.connector",
        fixture_stream_manager_kwargs["source"]["connector"],
        "--stream_manager.source.stream",
        fixture_stream_manager_kwargs["source"]["stream"],
        "--stream_manager.source.options",
        json.dumps(fixture_stream_manager_kwargs["source"]["options"]),
        # convert a SinkDescriptor to json for arg parser, which expects all args to be strings.
        "--stream_manager.sink.connector",
        fixture_stream_manager_kwargs["sink"]["connector"],
        "--stream_manager.sink.stream",
        fixture_stream_manager_kwargs["sink"]["stream"],
        "--stream_manager.sink.options",
        json.dumps(fixture_stream_manager_kwargs["sink"]["options"]),
    ]


@pytest.fixture
def fixture_stream_manager_config_file(tmp_path, fixture_stream_manager_kwargs):
    config_file_path = tmp_path / "config.yaml"

    with open(str(config_file_path), "w") as config_file:
        yaml.dump(
            {stream_manager.argparse_namespace: fixture_stream_manager_kwargs},
            config_file,
            default_flow_style=False,
        )

    return config_file_path


@pytest.fixture
def stream_with_error_sink(
    tmp_path,
    fixture_schema_uris,
    fixture_stream_config_uri,
    source_descriptor_file_test_event_example,
    test_event_enriched,
    test_event_error,
):
    main_output_path = tmp_path / "main_output"
    error_output_path = tmp_path / "error_output"

    return stream_manager(
        job_name="test_manager",
        schema_uris=fixture_schema_uris,
        stream_config_uri=fixture_stream_config_uri,
        source=source_descriptor_file_test_event_example,
        sink=SinkDescriptor(
            connector="file",
            stream=str(test_event_enriched),
            options={"uri": f"file://{str(main_output_path)}"},
        ),
        error_sink=SinkDescriptor(
            connector="file",
            stream=str(test_event_error),
            options={"uri": f"file://{str(error_output_path)}"},
        ),
        process_max_workers_default=2,
        process_batch_size_default=1,
    )
