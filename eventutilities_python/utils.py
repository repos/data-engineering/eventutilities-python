import glob
import logging
import os
from dataclasses import dataclass
from datetime import datetime
from typing import Collection, Dict, Any, Callable, Optional
from logging.config import dictConfig

import psutil
from py4j.java_gateway import JavaObject
from pyflink.common import Instant


def find_lib_jars(lib_dir: str) -> Collection[str]:
    """Returns a list of file:// prefixed .jar file paths in the directory."""
    pattern = os.path.join(lib_dir, "*.jar")
    return ["file://" + jar for jar in glob.glob(pattern)]


def format_dt(dt: datetime) -> str:
    """Formats dt into an ISO-8601 UTC ('Z') string."""
    return dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")


def flink_instant_of_datetime(dt: datetime) -> Instant:
    """Converts a python datetime to a PyFlink Instant."""
    return Instant.of_epoch_milli(int(dt.timestamp()) * 1000)


def json_default_serializer(obj: Any) -> Any:
    """A serializer function that handles a few more types that python json
    serializer doesn't know how to handle.

    Used when serializing objects to json that have types like datetimes
    or Flink Instants.
    """
    if isinstance(obj, datetime):
        return format_dt(obj)
    if isinstance(obj, Instant):
        return format_dt(datetime.utcfromtimestamp(float(obj.to_epoch_milli() / 1000.0)))
    raise TypeError("Type %s not serializable" % type(obj))


def callable_repr(func: Callable[..., Any]) -> str:
    """Given a callable, returns a nice description string using its name, file
    and lineno, if available."""
    desc = getattr(func, "__name__", repr(func))

    code = getattr(func, "__code__", None)
    if code:
        file = code.co_filename
        lineno = code.co_firstlineno
        desc = f"{desc} at {file}:{lineno}"
    return desc


def get_memory_usage_of(mem_info_key: str) -> int:
    """Calls psutil.Process().memory_full_info() and returns the memory usage
    bytes value for mem_info_key.

    :param mem_info_key:     See
    https://psutil.readthedocs.io/en/latest/#psutil.Process.memory_full_info
    :param mem_info_key: See https://psutil.readthedocs.io/en/latest/#ps
        util.Process.memory_full_info for a list of available keys.
    """
    return int(getattr(psutil.Process().memory_full_info(), mem_info_key))


def setup_logging(
    config_dict: Optional[Dict[Any, Any]] = None,
    level: str = "INFO",
) -> None:
    """Sets up python logging either from a log config file or using
    basicConfig.

    :param config_dict: Setup logging from a config dict. If not set,
        then basicConfig will be used.
    :param level: basicConfig log level that will be used if config_file is not set.
    """
    if config_dict:
        # version is always 1 according to
        # https://docs.python.org/3/library/logging.config.html#dictionary-schema-details
        config_dict['version'] = 1
        dictConfig(config_dict)
    else:
        logging.basicConfig(
            level=getattr(logging, level.upper()),
            datefmt="%Y-%m-%dT%H:%M:%S%z",
        )


@dataclass
class JavaObjectWrapper(object):
    """A wrapper class that maintains an object implemented in Java via py4j.

    Based on pyflink's JavaFunctionWrapper.
    """

    java_object: JavaObject
    """The underlying py4j JavaObject."""
