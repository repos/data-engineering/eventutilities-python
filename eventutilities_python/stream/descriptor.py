import re
from copy import deepcopy
from dataclasses import dataclass, field
from typing import Optional, Any, ClassVar, List, Dict, Union

from pyflink.common import WatermarkStrategy
from pyflink.datastream import (
    StreamExecutionEnvironment,
    DataStream,
    DataStreamSink,
    SinkFunction,
    SourceFunction,
)
from pyflink.datastream.connectors import Source, Sink

from eventutilities_python.flink import (
    EventDataStreamFactory,
    convert_pyflink_to_python,
    python_to_pyflink_converter,
)


@dataclass
class EventStreamDescriptor:
    """
    Source and sink streams have an associated event version.

    Example:
        >>> stream = EventStreamDescriptor(name="test.event.stream", version="1.1.0")
        >>> stream.name, stream.version
    """

    name: str
    version: Optional[str] = "latest"

    def __str__(self):
        return f"{self.name}:{self.version}"

    @staticmethod
    def is_valid_stream_name(stream_name):
        """Check if `stream_name` contains invalid characters.

        This method should be invoked every time we generate a stream name from
        the library (e.g. error_stream auto-detection).

        Stream name validation should not be enabled by default on all streams: We might
        want to consume streams that for historical reasons are now considered "invalid".
        Howeverm we should make sure that the streams generated
        by eventutilities-python are valid.

        :param stream_name: a stream name
        :return:
        """
        invalid_tokens = ["-"]
        for token in invalid_tokens:
            if token in stream_name:
                return False
        return True

    def is_version_pinned(self) -> bool:
        # This expression has type Union[str, None, bool].
        # Ignore type checking and coerce to bool
        return self.version and not self.version == "latest"  # type: ignore

    @staticmethod
    def from_string(
        stream_name: str,
    ) -> Any:  # TODO: how do we annotate a static method?
        """A factory method that parses `stream_name` and returns an instance
        of EventStream.

        Example:
            >>> stream = EventStreamDescriptor.from_string("test.event.stream:1.1.0")
            >>> stream.name, stream.version

        :param stream_name: a stream name with versioning scheme "<name>:<version>"
        """
        # Version and name validation are delegated to Java eventutilities
        try:
            match = re.search("^(?P<name>.*):(?P<version>.*)$", stream_name)
            name, version = match.groups()  # type: ignore
            if not name or not version:
                raise ValueError

        except (ValueError, TypeError, AttributeError) as e:
            raise ValueError(
                f"Invalid stream descriptor {stream_name}. Stream descriptor"
                f"must be declared as <stream_name>:<schema_version>. {e}"
            )
        return EventStreamDescriptor(name, version)


@dataclass
class ConnectorDescriptor:
    supported_connectors: ClassVar[List[str]] = []
    """List of supported connectors.

    These will be checked before instantiating a concrete Connector
    (Source or Sink).
    """

    connector: str
    """A supported connector name, e.g. "file" or "kafka"."""

    stream: str
    """An EventStreamDescriptor string, in the form of
    "stream_name:schema_version"."""

    name: Optional[str] = None
    """Name of the source or sink, will be used for UI and metrics."""

    options: Dict[Any, Any] = field(default_factory=dict)
    """Kwargs to pass to source or sink connector."""

    def __post_init__(self):
        self.stream_descriptor = EventStreamDescriptor.from_string(self.stream)
        self.name = self.name or f"{self.connector}__{self.stream_descriptor.name}"

        assert self.connector in self.supported_connectors, (
            f"{self.stream_descriptor} connector '{self.connector}' is not a "
            f"supported {self.__class__.__name__}. Must be one of {self.supported_connectors}."
        )

    def __str__(self):
        return f"{self.name}"


@dataclass
class SourceDescriptor(ConnectorDescriptor):
    supported_connectors: ClassVar[List[str]] = ["file", "collection", "kafka", "sse"]

    def source(self, datastream_factory: EventDataStreamFactory) -> Union[Source, SourceFunction]:
        """Instantiates a Flink Source.

        :param datastream_factory:
            An EventDataStreamFactory that will be used to
            create the Flink source using this SourceDescriptor.
        """

        if self.connector == "file":
            flink_source = datastream_factory.file_source(
                stream_name=self.stream_descriptor.name,
                schema_version=self.stream_descriptor.version,
                **self.options,
            )
        elif self.connector == "collection":
            flink_source = datastream_factory.collection_source(
                stream_name=self.stream_descriptor.name,
                schema_version=self.stream_descriptor.version,
                **self.options,
            )
        elif self.connector == "kafka":
            flink_source = datastream_factory.kafka_source(
                stream_name=self.stream_descriptor.name,
                schema_version=self.stream_descriptor.version,
                **self.options,
            )
        # Excluded from coverage (for now) because it is difficult to test.
        elif self.connector == "sse":  # pragma: no cover
            flink_source = datastream_factory.sse_source(
                stream_name=self.stream_descriptor.name,
                schema_version=self.stream_descriptor.version,
                **self.options,
            )
        # NOTE: else assertion is handled by ConnectorDescriptor.__post_init__

        return flink_source

    def datastream(
        self,
        env: StreamExecutionEnvironment,
        datastream_factory: EventDataStreamFactory,
        watermark_strategy: Optional[WatermarkStrategy] = None,
        convert_to_native_python: bool = True,
    ) -> DataStream:
        """Instantiates a Flink DataStream reading from source.

        :param env:
            Flink env with required Java dependencies loaded.

        :param datastream_factory:
            An EventDataStreamFactory that will be used to
            create the Flink source using the SourceDescriptor.

        :param watermark_strategy:
            WatermarkStrategy to use for the DataStream source.
            Defaults to for_monotonous_timestamps() if connector is Kafka,
            else no_watermarks() will be used.

        :param convert_to_native_python:
            If True, the datastream will be converted from Flink types
            to native python types (dicts, datetimes, etc.) before being returned.

        """

        if not watermark_strategy:
            if self.connector == "kafka":
                watermark_strategy = WatermarkStrategy.for_monotonous_timestamps()
            else:
                watermark_strategy = WatermarkStrategy.no_watermarks()

        datastream = datastream_factory.from_source(
            env=env,
            stream_name=self.stream_descriptor.name,
            schema_version=self.stream_descriptor.version,
            source=self.source(datastream_factory),
            watermark_strategy=watermark_strategy,
            source_name=f"source_{self.name}",
        ).name(self.name)
        # TODO uid?

        if convert_to_native_python:
            datastream = datastream.map(convert_pyflink_to_python).name(
                f"{self.name}__convert_flink_to_python"
            )
            # TODO: uid?

        return datastream


@dataclass
class SinkDescriptor(ConnectorDescriptor):
    supported_connectors: ClassVar[List[str]] = ["null", "file", "kafka", "print"]
    """
    Note: "null" is a special case, and not a real sink.
    It causes both sink and sink_to to return None.
    """

    def __post_init__(self):
        """If connector is kafka, one of "topic" or "kafka_topic_prefix" is
        required in options.

        If topic is not given, "kafka_topic_prefix" will be used to
        create the "topic" like kafka_topic_prefix +
        stream_descriptor.name
        """
        super().__post_init__()
        if self.connector == "kafka" and "topic" not in self.options:
            # Set a default kafka_topic_prefix as empty string.
            self.options.setdefault("kafka_topic_prefix", "")
            self.options["topic"] = self.options["kafka_topic_prefix"] + self.stream_descriptor.name
            # NOTE: we'd like to delete kafka_topic_prefix from options now, but
            # we want to be able to make a copy of this descriptor and modify it if we need to,
            # so we keep it for now, and just avoid passing when creating the actual kafka Sink.

    def sink(
        self, datastream_factory: EventDataStreamFactory
    ) -> Optional[Union[Sink, SinkFunction]]:
        """Instantiates a Flink Sink.

        :param datastream_factory: An EventDataStreamFactory that will
                be used to             create the Flink source using the
                SourceDescriptor             configuration.
        """

        if self.connector == "null":
            flink_sink = None

        elif self.connector == "file":
            flink_sink = datastream_factory.row_file_sink(
                self.stream_descriptor.name,
                self.stream_descriptor.version,
                **self.options,
            )

        elif self.connector == "kafka":
            # kafka_topic_prefix is not a real kafka_sink option, so remove it from
            # the options kwarg we pass to it.
            sink_options = deepcopy(self.options)
            if "kafka_topic_prefix" in sink_options:
                del sink_options["kafka_topic_prefix"]

            flink_sink = datastream_factory.kafka_sink(
                stream_name=self.stream_descriptor.name,
                schema_version=self.stream_descriptor.version,
                **sink_options,
            )

        elif self.connector == "print":  # pragma: no cover
            flink_sink = datastream_factory.print_sink(self.stream_descriptor)

        # NOTE: else assertion is handled by ConnectorDescriptor.__post_init__

        return flink_sink

    def sink_to(
        self,
        datastream_factory: EventDataStreamFactory,
        datastream: DataStream,
        convert_to_pyflink: bool = True,
    ) -> Optional[DataStreamSink]:
        """Instantiates a Flink Sink and sinks the datastream to it.

        :param datastream_factory:
            An EventDataStreamFactory that will be used to create the Flink source
            using the SourceDescriptor configuration.

        :param datastream:
            the datastream to write to the Sink.  This datastream
            MUST conform to the shape (TypeInformation) of the Sink, which is set
            by the schema of this SinkDescriptor's stream_descriptor.

        :param convert_to_pyflink:
            If True, it is expected that the elements in datastream have
            previously been converted to native python types. They will
            be converted back to PyFlink types (Rows, etc.)
            using the Sink's TypeInformation before being written to the Sink.
            This means that the element types must conform to the stream_descriptor event schema.
        """
        sink = self.sink(datastream_factory)

        if sink is None:
            sink_result = None

        if convert_to_pyflink:
            sink_type_info = datastream_factory.get_row_type_info(
                self.stream_descriptor.name,
                self.stream_descriptor.version,
            )
            sink_converter = python_to_pyflink_converter(sink_type_info)
            datastream = datastream.map(sink_converter, sink_type_info).name(
                f"convert_python_to_pyflink__${self.name}"
            )  # TODO uid?

        if isinstance(sink, SinkFunction):
            sink_result = datastream.add_sink(sink).name(self.name)  # TODO uid?
        elif isinstance(sink, Sink):
            sink_result = datastream.sink_to(sink).name(self.name)  # TODO uid?

        return sink_result


# Export public API symbols
__all__ = [
    "EventStreamDescriptor",
    "SourceDescriptor",
    "SinkDescriptor",
]
