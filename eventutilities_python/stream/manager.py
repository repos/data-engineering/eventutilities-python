import os
from concurrent.futures import ThreadPoolExecutor, Future
from datetime import datetime
from functools import partial
from time import perf_counter
from typing import (
    Any,
    Callable,
    Dict,
    List,
    Optional,
    Tuple,
    Union,
    Sequence,
    Generator,
)

import psutil
from pyflink.metrics import Counter

try:
    from typing import Self  # type: ignore
except ImportError:
    # Can be removed in later versions of python/mypy?
    from typing_extensions import Self

from copy import deepcopy
from jsonargparse import ArgumentParser, ActionConfigFile, Namespace
import logging
import traceback
from abc import ABC, abstractmethod

from pyflink.common import Row
from pyflink.datastream import StreamExecutionEnvironment
from pyflink.datastream.functions import (
    KeyedProcessFunction,
    RuntimeContext,
)
from pyflink.datastream.output_tag import OutputTag


from eventutilities_python.flink import (
    EventDataStreamFactory,
    get_flink_env,
)
from eventutilities_python.stream.descriptor import (
    EventStreamDescriptor,
    ConnectorDescriptor,
    SourceDescriptor,
    SinkDescriptor,
)
from eventutilities_python.stream.error import error_schema_version, create_error_event
from eventutilities_python.utils import (
    callable_repr,
    setup_logging,
    get_memory_usage_of,
    flink_instant_of_datetime,
)
from eventutilities_python.type_aliases import (
    CloseableIterator,
    EventDictFilterFunction,
    EventDictMapFunction,
    UriAbsolute,
    Path_uc,
    resolve_uris,
    EventDict,
)


# TODO: Remove these hardcoded defaults and put them in config files?
#       Or perhaps defaults to apply in load_config.
default_schema_uris: Tuple[str, ...] = (
    "https://schema.wikimedia.org/repositories/primary/jsonschema",
    "https://schema.wikimedia.org/repositories/secondary/jsonschema",
)
"""WMF schema URIs that work outside of WMF internal production networks.

In production, you should use the internal endpoints, or even better,
local clones of the schema repositories.
"""

default_stream_config_uri: str = "https://meta.wikimedia.org/w/api.php"
"""WMF MediaWiki stream config URI.

In production, you should use the internal API endpoints and set
http_client_routes appropriately.
"""


def load_config(
    argv: Optional[List[str]] = None,
    parser: Optional[ArgumentParser] = None,
    defaults: Optional[Dict[Any, Any]] = None,
    default_config_files: Optional[List[str]] = None,
) -> Namespace:
    """Returns a jsonargparse Namespace object with all configured classes
    instantiated from configs.

    Notably, the returned config.stream_manager will
    contain all the kwargs necessary to call the stream_manager constructor.

    :param argv:
        List of args to parse into config.
        If this is None, jsonsargparse will use sys.argv.

    :param parser:
        A pre-instantiated ArgumentParser.  Pass this in if you have
        custom arguments to configure.  If None,
        a default ArgumentParser will be constructed.

    :param defaults:
        Default values to apply when parsing args.

    Example:
        >>> parser = ArgumentParser()
        >>> parser.add_argument('--my_custom_arg', type=str)
        >>> config = load_config(parser=parser)
        >>> with stream_manager.from_config(config) as stream: # ...
    """

    # Here we set env_prefix, but we don't enable env var parsing
    # by default. You can set JSONARGPARSE_DEFAULT_ENV=true to enable this.
    # See: https://jsonargparse.readthedocs.io/en/v4.20.0/#environment-variables
    # Setting env_prefix means if env parsing is enabled, all env arg variables will
    # be prefixed with this, e.g. EVENTUTILITIES_CONFIG.
    parser = parser or ArgumentParser(
        env_prefix="EVENTUTILITIES",
    )

    # Add stream_manager args. If we move load_config to a more general util,
    # we should either conditionally enable this, or ask stream_manager users
    # to do this manually.
    parser = stream_manager.arg_parser(parser)
    # TODO: consider using jsonargparse.capture_parser to work around some awkwardness.
    # TODO: consider using jsonargparse parsers as arguments to work around some awkwardness.
    #   https://jsonargparse.readthedocs.io/en/v4.20.0/#parsers-as-arguments
    #

    # Add ability to load config from files.
    parser.add_argument(
        "--config",
        action=ActionConfigFile,
    )

    parser.add_argument(
        "--log_config",
        help="Log configuration in dictionary format.",
        type=Optional[dict],
    )

    parser.default_config_files = default_config_files or []

    if defaults:
        parser.set_defaults(defaults)

    parsed_args = parser.parse_args(argv)
    config = parser.instantiate_classes(parsed_args)

    logging.info("Loaded config:\n\n%s", parser.dump(parsed_args))

    setup_logging(config.get("log_config"))

    return config


class stream_manager:
    argparse_namespace: str = "stream_manager"
    """Prefix namespace key in which arg_parser expects stream_manager
    configs.."""

    @classmethod
    def arg_parser(cls, parser: Optional[ArgumentParser] = None) -> ArgumentParser:
        """Returns an ArgumentParser which can be used to auto instantiate a
        stream_manager from CLI and/or config files. You can augment this
        parser with your own CLI opts if you need to.

        :param parser: If not given, a new ArgumentParser will be created.
        """

        parser = parser or ArgumentParser()

        # Add all stream_manager constructor params to arg_parser in the
        # stream_manager namespace.  This allows
        # to use jsonargparse instantiate_classes to automatically instantiate
        # any dataclass params from parsed args.
        # NOTE: instantiate=False so that instantiate_classes will not
        # automatically create stream_manager instances.  To do that,
        # use from_config with the parsed Namespace config object instead.
        # NOTE: Unfortunately, dataclass field pydocs are not supported by python,
        # so jsonargparse cannot automatically use the dataclass field docs
        # in the help documentation :/
        parser.add_class_arguments(cls, cls.argparse_namespace, instantiate=False)
        return parser

    @classmethod
    def from_config(cls, config: Namespace) -> "stream_manager":
        """Instantiate stream_manager with a jsonargparsed config Namespace
        (with instantiated dataclass params).

        Example:
            >>> stream_manager.from_config(stream_manager.arg_parser())
            or more generally:
            >>> stream_manager.from_config(load_config())
        """

        # Because we accept absolute URIs as some of our
        # connector options, we need to resolve these if they are
        # relative paths in config files.  This is a bit hacky.
        # If we instead had actual dataclass params for each
        # connector option, we could declare the types of those
        # dataclass params as jsonargparse path_type('u') and
        # allow jsonargparse to resolve URIs relative to the
        # config file they are declared in.
        #
        # Instead, attempt to resolve any relevant connector options
        # relative to all used config files.

        # Resolve these options if any ConnectorDescriptor has them set.
        options_needing_uri_resolving = ["uris", "uri"]

        # Attempt to resolve the uri options in reverse order of --config
        # (later --config have more precedence), and finally against cwd.
        # config.config are the --config files
        config_files = config.config or []
        resolve_relative_to = list(reversed(config_files)) + [Path_uc(os.getcwd())]

        # For each ConnectorDescriptor in config
        for connector in [
            c for c in config[cls.argparse_namespace].values() if isinstance(c, ConnectorDescriptor)
        ]:
            # If connector has an options_needing_uri_resolving
            for option_key in [k for k in options_needing_uri_resolving if k in connector.options]:
                connector.options[option_key] = resolve_uris(
                    connector.options[option_key],
                    resolve_relative_to,
                )
                logging.debug(
                    "Resolved %s %s URI option '%s' to '%s'",
                    type(connector).__name__,
                    connector,
                    option_key,
                    connector.options[option_key],
                )

        return cls(**config[cls.argparse_namespace])

    @staticmethod
    def check_valid_stream_version(stream_descriptor: EventStreamDescriptor) -> None:
        if not stream_descriptor.is_version_pinned():
            raise ValueError(
                f"{stream_descriptor} - 'latest' is not a valid version."
                "An event version must be declared as $major.$minor.$patch"
            )

    def __init__(
        self,
        job_name: str,
        source: SourceDescriptor,
        sink: SinkDescriptor,
        # NOTE: We'd prefer to just set error_sink = None, and/or
        # support 'auto error sink stream' from a str, but
        # using a Union/Optional type means that jsonargparse
        # can't properly support 'dataclass' style configuration.
        # This is awkward, so we work around this by setting
        # a default dummy "null" SinkDescriptor to support
        # the 'optional' error sink behavior.
        error_sink: SinkDescriptor = SinkDescriptor(
            connector="null",
            stream=f"stream.error:{error_schema_version}",
        ),
        schema_uris: Union[
            Sequence[UriAbsolute], UriAbsolute
        ] = default_schema_uris,  # type: ignore
        stream_config_uri: UriAbsolute = default_stream_config_uri,  # type: ignore
        http_client_routes: Optional[Dict[str, str]] = None,
        process_max_workers_default: int = 12,
        process_batch_size_default: int = 12,
        process_batch_duration_ms_default: int = 2 * 60 * 1000,
    ):
        """A context manager that wraps the setup of a Flink stream pipeline
        with a Kafka source and sink with eventutilities stream configs.

        :param schema_uris:
            Event schema repository URIs.  Event schemas will be loaded from these.
            Either a list or a csv.

        :param stream_config_uri:
            URI from which to get stream config.
            See org.wikimedia.eventutilities.core.event.EventStreamConfig

        :param http_client_routes:
            A mapping of source hostnames to dest hostnames.  If an HTTP request is to made
            to a source hostname, the request will be sent to dest hostname but with the
            Host header set to source hostname.  Used in WMF production environments
            where we need to use discovery MediaWiki API endpoints, e.g. api-ro.discovery.wmnet

        :param job_name:
            The name of the streaming job. Will be used in UI and metric names.

        :param source:
            SourceDescriptor.  Will be used to instantiate
            and then read from sources.

        :param sink:
            SinkDescriptor configuration.  Will be used to instantiate
            and write the final output. The shape of the final output
            must conform to sink.stream's event schema.

        :param error_sink:
            Error SinkDescriptor config.  If set, any errors encountered
            during processing will be written to this sink.
            You must ensure that the error stream is declared in Event Stream Config.

            The default is a dummy "null" SinkDescriptor connector,
            which will cause no error sink to be used.
            In this case, errors will be raised to the caller.

        :param process_max_workers_default:
            Max number of thread workers to use when using async
            stream.process with a python function.

        :param process_batch_size_default:
            Batch size to use when using async stream.process with a python function.

        :param process_batch_duration_ms_default:
            Batch duration in ms when using async stream.process with a python function.
        """

        # Will be initialized in __enter__, and closed in __exit__.
        self.flink_env: StreamExecutionEnvironment

        self.job_name = job_name

        # If needed, convert schema_uris to a list from a csv
        if isinstance(schema_uris, str):
            schema_uris = schema_uris.split(",")  # type: ignore

        self.schema_uris = schema_uris
        self.stream_config_uri = stream_config_uri
        self.http_client_routes = http_client_routes or {}

        self.process_max_workers_default = process_max_workers_default
        self.process_batch_size_default = process_batch_size_default
        self.process_batch_duration_ms_default = process_batch_duration_ms_default

        self.source_descriptor = source
        self.check_valid_stream_version(self.source_descriptor.stream_descriptor)

        self.sink_descriptor = sink
        self.check_valid_stream_version(self.sink_descriptor.stream_descriptor)

        # error_sink.connector == "null" is a valid 'SinkDescriptor", but
        # we'd prefer to just have logic here in manager to avoid using
        # an error sink at al all (no side output) if we don't need it.
        if error_sink.connector == "null":
            self.error_sink_descriptor = None
        else:
            self.error_sink_descriptor = error_sink
            self.check_valid_stream_version(self.error_sink_descriptor.stream_descriptor)

    def __enter__(self):
        # Call get_flink_env() to ensure that
        # eventutilities java deps are loaded in flink env at runtime.
        self.flink_env = get_flink_env()
        datastream_factory = EventDataStreamFactory.of(
            self.schema_uris,
            self.stream_config_uri,
            self.http_client_routes,
        )

        # Validate that all the streams we will use exist in event stream config
        for connector_descriptor in [
            self.source_descriptor,
            self.sink_descriptor,
            self.error_sink_descriptor,
        ]:
            if connector_descriptor and not datastream_factory.stream_exists(
                connector_descriptor.stream_descriptor.name
            ):
                raise ValueError(
                    connector_descriptor.stream_descriptor.name
                    + f" is not declared in stream config {self.stream_config_uri}"
                )

        logging.info(
            f"stream manager will consume from {self.source_descriptor}, "
            f" and produce to {self.sink_descriptor}."
        )
        if self.error_sink_descriptor:
            logging.info(
                f"stream manager will produce error events to {self.error_sink_descriptor}"
            )

        stream = FlinkStream(
            env=self.flink_env,
            datastream_factory=datastream_factory,
            job_name=self.job_name,
            source_descriptor=self.source_descriptor,
            sink_descriptor=self.sink_descriptor,
            error_sink_descriptor=self.error_sink_descriptor,
            process_max_workers_default=self.process_max_workers_default,
            process_batch_size_default=self.process_batch_size_default,
            process_batch_duration_ms_default=self.process_batch_duration_ms_default,
        )
        return stream

    def __exit__(self, exc_type, exc_value, tb):
        self.flink_env.close()

        if exc_value:
            logging.error(exc_value)
            traceback.print_tb(tb)
            raise exc_value

    def __str__(self):
        return (
            f"{self.__class__.__name__} {self.job_name}: "
            f"{self.source_descriptor} -> {self.sink_descriptor}"
        )


class Stream(ABC):
    """Stream is an interface that implies operations on a datastream.

    It defines the API contract with users of eventutilities-python.
    """

    @abstractmethod
    def filter(self, func: Callable[[Any], Any], name: Optional[str] = None) -> Any:
        pass

    @abstractmethod
    def partition_by(self, func: Callable[[Any], Any]) -> Any:
        pass

    @abstractmethod
    def process(self, func: Callable[[Any], Any], name: Optional[str] = None) -> Any:
        pass

    @abstractmethod
    def execute(self) -> Any:
        pass


class FlinkStream(Stream):
    """An Apache Flink Event Platform stream pipeline builder."""

    def __init__(
        self,
        env: StreamExecutionEnvironment,
        datastream_factory: EventDataStreamFactory,
        job_name: str,
        source_descriptor: SourceDescriptor,
        sink_descriptor: SinkDescriptor,
        error_sink_descriptor: Optional[SinkDescriptor] = None,
        process_max_workers_default: int = 12,
        process_batch_size_default: int = 12,
        process_batch_duration_ms_default: int = 2 * 60 * 1000,
    ):
        """Set up the stream and delegate transformations to a Flink Streaming
        environment.

        :param env: an instance of Flink StreamExecutionEnvironment
        :param datastream_factory: EventDataStreamFactory instance.
        :param job_name: Name of the Flink job.
        :param source_descriptor:
        :param sink_descriptor: Sink in which to write final output datastream
        :error_sink_descriptor: Sink in which error events will be written to
        :param process_max_workers_default: Max number of thread workers
                to use when using async             stream.process with
                a python function.
        :param process_batch_size_default: Batch size to use when using
                async stream.process with a python function.
        :param process_batch_duration_ms_default: Batch duration in ms
                when using async stream.process with a python function.
        """
        self.env = env
        self.datastream_factory = datastream_factory
        self.job_name = job_name
        self.source_descriptor = source_descriptor
        self.sink_descriptor = sink_descriptor
        self.error_sink_descriptor = error_sink_descriptor

        self.process_max_workers_default = process_max_workers_default
        self.process_batch_size_default = process_batch_size_default
        self.process_batch_duration_ms_default = process_batch_duration_ms_default

        # Convert the source DataStream to a python dict
        # so users can work with it more easily.
        self.datastream = self.source_descriptor.datastream(
            self.env,
            self.datastream_factory,
            convert_to_native_python=True,
        )

    def filter(self, func: EventDictFilterFunction, name: Optional[str] = None) -> Self:
        """Applies a filter function to a datastream.

        :param func: a Python Callable
        :param name: Will be used as the Flink operator name and in metrics
        :return:
        """
        name = name or callable_repr(func)
        operator_name = f"filter_{name}"

        self.datastream = self.datastream.filter(func).name(operator_name)
        return self

    def partition_by(self, func: Callable[[Any], Any]) -> Self:
        self.datastream = self.datastream.key_by(func)
        return self

    def process(self, func: EventDictMapFunction, name: Optional[str] = None) -> Self:
        """Applies a map function with metrics and error handling.

        :param func: a Python Callable
        :param name: Will be used as the Flink operator name and in metrics
        :return:
        """

        # Process the func
        # using the ErrorHandlingProcessFunction, possibly emitting
        # any encountered errors to the error_sink.

        name = name or callable_repr(func)

        operator_name = f"process_{name}"
        error_operator_name = f"process_error_{name}"

        if self.error_sink_descriptor:
            error_output_tag = OutputTag(error_operator_name)
        else:
            error_output_tag = None

        self.datastream = self.datastream.process(
            EventProcessFunction(
                func=func,
                name=operator_name,
                # Use the name of the Flink job as
                # the 'emitter_id' in any emitted error events.
                emitter_id=self.job_name,
                # if error_output_tag is None, no error side-output will be produced.
                error_output_tag=error_output_tag,
                max_workers=self.process_max_workers_default,
                batch_size=self.process_batch_size_default,
                batch_duration_ms=self.process_batch_duration_ms_default,
            )
        ).name(operator_name)

        # If error_sink is defined, send the error_datastream to the sink now.
        if self.error_sink_descriptor:
            # NOTE: datastream.get_side_output()
            # MUST be called after datastream.process, if the
            # ProcessFunction emits to any side output.  Otherwise, the
            # side output records will remain in the main datastream
            # as Tuple(output_tag, value).
            error_datastream = self.datastream.get_side_output(error_output_tag).name(
                error_operator_name
            )

            self.error_sink_descriptor.sink_to(self.datastream_factory, error_datastream)

        return self

    def execute(self, collect: bool = False) -> Optional[Union[CloseableIterator, List[Row]]]:
        """Triggers the program execution.

        :param collect: If True, data_stream.execute_and_collect() will
                be returned, otherwise             the pipeline will
                just be executed.
        :return:
        """
        if self.sink_descriptor:
            self.sink_descriptor.sink_to(
                self.datastream_factory,
                self.datastream,
                convert_to_pyflink=True,
            )

        results = None
        if collect:
            results = self.datastream.execute_and_collect(job_execution_name=self.job_name)
        else:
            self.env.execute(job_name=self.job_name)

        return results


class EventProcessFunction(KeyedProcessFunction):
    """
    A ProcessFunction that:

    - Calls a user provided function, expected to take a single event dict,
      and return a transformed event dict.

    - Implements a thread pool on a minibatch to allow asynchronous function execution.
        A batch is considered full when one the following conditions are met:
            1. `batch_size` elements have been consumed.
            2 . and interval `batch_duration_ms` milliseconds have elapsed.
      Mini batching with count and time triggers combines two KeyedProcessFunction methods to
      mini-batch a datastream using windows:
        1. `process_elements()` consumes incoming records, and submits futures to an
            executor thread pool. Once `batch_size` records have been consumed, futures are
            collected and the current window is closed.
            When `process_elements()` is invoked and no future has been scheduled
            a timer is registered in Flink Timer Registry. The timer will trigger
            at <current_time> + `batch_duration_ms`.
        2. `on_timer()` is triggered when the window timeout is reached and the timer registered
            in `process_elements()` fires. Any pending future is collected and the current window
            is closed.
        TODO: we should consider extracting this logic out of a Process function
        and move it to a trigger.
        Currently there is tight coupling with async execution and our EventProcessFunction logic,
        since it is our public API and we don't need users to be concerned with partitioning
        and managing windows. Future use cases could benefit from a reusable trigger.
        Maybe part of the Java codebase?
    - Emits various Flink metrics about memory used, and process func results.

    - If configured to do so, converts encountered Exceptions to
      WMF Event Platform error events (as dicts) and
      produces those error event dicts to the provided output_tag.

    NOTE: If error_output_tag is provided,  you MUST call get_side_output(output_tag)
    on the resulting DataStream, otherwise the side output events will remain
    in the main DataStream, which will cause errors if the side output events are
    of a different type than the main DataStream.
    """

    metric_in_total_name = "events_in_total"
    """Total count of input events."""

    metric_out_total_name = "events_out_total"
    """Total count of output events."""

    metric_success_total_name = "events_success_total"
    """Total count of successfully processed events."""

    metric_error_total_name = "events_error_total"
    """Total count of events that resulted in process errors.

    If error_output_tag is enabled, then this should equal the number of
    error events produced to the side output.
    """

    # Durations would better be measured by Histograms, or
    # 'Distributions' in PyFlink.  In my testing,
    # Distributions are just converted to Gauges, and in
    # Prometheus, never reported with real values.
    # Using a counter to keep the total time spent
    # invoking the user's process func, and we
    # can then calculate average rates on that.
    # https://nightlies.apache.org/flink/flink-docs-master/docs/dev/python/table/metrics/#distribution
    # https://github.com/apache/flink/blob/master/flink-python/src/main/java/org/apache/flink/python/metric/embedded/MetricDistribution.java
    metric_invocation_ms_total_name = "invocation_milliseconds_total"
    """Total amount of time spent in the user provided process function.

    Since this uses async mini batching, this timer is tracked per
    batch. It is the amount of time spent waiting for the async
    functions in the batch to complete.
    """

    # NOTE: we also emit memory_{mem_info_key}_bytes
    # Latest value of memory bytes used. Data is provided by
    # psutil.Process().memory_full_info(), so actual
    # metrics emitted will match the keys for whatever that returns.

    def __init__(
        self,
        func: EventDictMapFunction,
        name: str,
        emitter_id: str,
        error_output_tag: Optional[OutputTag] = None,
        max_workers: int = 12,
        batch_size: int = 12,
        batch_duration_ms: int = 60 * 2 * 1000,
    ):
        """:param func: User provided transform function. This function should
        take an event dict, and return an event dict. If used at the end of a
        DataStream pipeline, it is important that the returned event dict
        matches the schema RowTypeInfo of the Row it will be converted back to.

        :param name: Will be used for the metric group name for Flink metrics.
        :param emitter_id: Error event emitter_id field. Only used if
                producing error events.
        :param error_output_tag: If provided, this is the OutputTag to
                which error events will be emitted.             If None,
                no error events will be written to OutputTags, and
                any encountered Exceptions will be re-raised.
        :param max_workers: maximum amount of threads in an executor pool.
        :param batch_size: number of records to collect per batch (Flink window)
        :param batch_duration_ms: maximum amount of time, in
                milliseconds, to wait for             a batch (Flink
                window) to be filled.
        """
        self.func = func
        self.name = name
        self.emitter_id = emitter_id

        if error_output_tag:
            self.error_output_tag = error_output_tag
        else:
            self.error_output_tag = None

        # Counter metrics will be stored in this dict, rather than
        # as individual instance variables.
        self.counter_metrics: Dict[str, Counter] = {}

        self.max_workers = max_workers
        self.batch_size = batch_size
        self.batch_duration_ms = batch_duration_ms

        # Declare this here, it will be instantiated in open().
        self.executor: ThreadPoolExecutor

        # timestamp that determines when a timer will be triggered, ending the window.
        self.trigger_timestamp = 0

        # batch_futures keep track of futures scheduled
        # during a window lifetime. This dict keeps an inverted index
        # of a record and the future that will be process it.
        self.batch_futures: List[Tuple[Future[EventDict], EventDict]] = []

    def init_metrics(self, runtime_ctx: RuntimeContext) -> None:
        """Initialize Flink metrics for this ProcessFunction."""
        # In Prometheus, this will make metrics like:
        # flink_taskmanager_job_task_operator_event_process_function_<metric_name>
        #   {event_process_function=<self.name>, ... }

        metrics_group = runtime_ctx.get_metrics_group().add_group(
            "event_process_function",
            self.name,
        )

        self.counter_metrics[self.metric_in_total_name] = metrics_group.counter(
            self.metric_in_total_name
        )
        self.counter_metrics[self.metric_success_total_name] = metrics_group.counter(
            self.metric_success_total_name
        )
        self.counter_metrics[self.metric_error_total_name] = metrics_group.counter(
            self.metric_error_total_name
        )
        self.counter_metrics[self.metric_out_total_name] = metrics_group.counter(
            self.metric_out_total_name
        )
        self.counter_metrics[self.metric_invocation_ms_total_name] = metrics_group.counter(
            self.metric_invocation_ms_total_name
        )

        # We will emit metrics for all fields returned by memory_full_info().
        # The list of memory metrics is dynamic because not all
        # mem_info fields are available on all operating systems.
        for mem_info_key in psutil.Process().memory_full_info()._asdict().keys():
            mem_metrics_group = metrics_group.add_group("memory_info", mem_info_key)
            mem_metrics_group.gauge(
                f"usage_bytes",
                partial(get_memory_usage_of, mem_info_key),
            )

    def open(self, runtime_ctx: RuntimeContext) -> None:
        self.init_metrics(runtime_ctx)
        # Here I'm using a ThreadPoolExecutor because:
        # 1. it does not require modifying the enrich function too much.
        # 2. enrichment is I/O bound (we need to wait on slow HTTP endpoints).
        # 3. we can exploit multi core k8s container.
        # Once we collect some perf data, we might want to experiment
        # with asyncio/aiohttp too.
        #
        # Use the default number worker threads, that is num. CPUs +4.
        # We can increase the number of threads via the max_workers param.
        self.executor = ThreadPoolExecutor(max_workers=self.max_workers)

    def _handle_process_error(
        self,
        e: Exception,
        event: EventDict,
    ) -> Generator[Tuple[OutputTag, EventDict], None, None]:
        """Log the error, increment error counter metric, and if
        error_output_tag is set, produce an error event to the error output tag
        side output.

        Otherwise, re-raise the error.
        """

        logging.exception(
            "Encountered exception while in ProcessFunction %s calling %s",
            self.name,
            callable_repr(self.func),
        )
        self.counter_metrics[self.metric_error_total_name].inc()

        if self.error_output_tag:
            # Create an error event representing this error.
            # Note that we do not set the error event stream name here.
            # It is expected that the JsonEventGenerator serializer
            # will be used, which handles setting of the stream name.
            error_event = create_error_event(
                error=e,
                errored_event=event,
                # TODO: Testing would be better if this process function
                #        was deterministic.  Using datetime.now() is not.
                #        Maybe take an error_dt_generator function?
                # Use an Instant here so that later on Flink sinks
                # will know how to serialize this properly.
                error_dt=flink_instant_of_datetime(datetime.now()),
                emitter_id=self.emitter_id,
            )
            yield self.error_output_tag, error_event

        else:
            # If we aren't capturing errors in error event side-output,
            # the re-raise the Exception and allow the job to fail.
            raise e

    def _collect(
        self,
        batch_futures: List[Tuple["Future[EventDict]", EventDict]],
    ) -> Generator[Union[EventDict, Tuple[OutputTag, EventDict]], None, None]:
        """Sequentially iterates over batch_futures and waits for each one to
        finish. Yields result as soon as the current future has finished.

        If an exception is encountered, _handle_process_error will be
        called.
        """

        # Invoke (and time) the function mini-batch
        start_time = perf_counter()

        # Iterate over the completed futures according to their insertion order.
        # `batch_futures` now contains both done and uncompleted futures returned
        # by wait().
        for future, event in batch_futures:
            try:
                result = future.result()
                if result is None:
                    raise ValueError(f"'{result}': invalid EventProcessFunction output record.")
                yield result
                self.counter_metrics[self.metric_success_total_name].inc()
            except Exception as e:
                yield from self._handle_process_error(e, event)
            finally:
                self.counter_metrics[self.metric_out_total_name].inc()

        invocation_ms = int((perf_counter() - start_time) * 1000)
        self.counter_metrics[self.metric_invocation_ms_total_name].inc(invocation_ms)

    def _close_batch(self):
        """Either `batch_size` records have been processed, or
        `batch_duration_ms` as elapsed. The current batch can be closed.

        Return list of batch futures and reset the callback state. The
        returned list of futures should be waited for and yielded.

        :return: a collection o scheduled futures
        """
        batch_futures = self.batch_futures
        self.batch_futures = []
        return batch_futures

    def close(self):
        self.executor.shutdown()

    def on_timer(
        self,
        timestamp: int,
        ctx: "KeyedProcessFunction.OnTimerContext",
    ) -> Generator[Union[EventDict, Tuple[OutputTag, EventDict]], None, None]:
        yield from self._collect(self._close_batch())

    def process_element(
        self,
        event: EventDict,
        ctx: KeyedProcessFunction.Context,
    ) -> Generator[Union[EventDict, Tuple[OutputTag, EventDict]], None, None]:
        self.counter_metrics[self.metric_in_total_name].inc()

        # batch_futures is empty, meaning we are starting a new batch here.
        # start a timer for this batch.
        if not self.batch_futures:
            self.trigger_timestamp = (
                ctx.timer_service().current_processing_time() + self.batch_duration_ms
            )
            ctx.timer_service().register_processing_time_timer(self.trigger_timestamp)

        # Run func in an executor thread, and add the (future, input event) tuple to the batch
        self.batch_futures.append(
            (
                self.executor.submit(self.func, deepcopy(event)),  # TODO: do we need to deepcopy?
                event,
            )
        )

        if len(self.batch_futures) >= self.batch_size:
            # The batch is full. Cancel the outstanding batch timer
            # and collect and emit the results.
            ctx.timer_service().delete_processing_time_timer(self.trigger_timestamp)
            yield from self._collect(self._close_batch())


if __name__ == "__main__":
    setup_logging(level=os.environ.get("LOG_LEVEL", "INFO"))
    logging.info("Loading and printing configuration, then exiting.")
    load_config()


# Export public API symbols
__all__ = [
    "stream_manager",
    "load_config",
]
