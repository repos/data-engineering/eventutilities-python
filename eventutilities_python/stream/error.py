import json
import uuid
from datetime import datetime
from traceback import TracebackException
from typing import Any, Dict, Optional, Union

import pydash
from pyflink.common import Instant

from eventutilities_python.utils import format_dt, json_default_serializer

error_schema_version = "2.1.0"
error_schema_uri = f"/error/{error_schema_version}"


def create_error_event(
    error: Union[Exception, str],
    emitter_id: str,
    error_dt: Union[datetime, str, Instant],
    error_stream_name: Optional[str] = None,
    errored_event: Optional[Union[Dict[str, Any], str]] = None,
) -> Dict[str, Any]:
    """An error that can be serialized into a WMF error event.
    https://schema.wikimedia.org/#!/primary/jsonschema/error.

    :param error:
        An Exception or error message

    :param emitter_id:
        Informative name of the 'emitter' of this error.

    :param error_dt:
        event time to use for the error event.
        Note that this is not the same as the event time
        of the errored_event which caused the error.
        - If this is a datetime, it will be formatted into an ISO-8601 'Z' string.
        - If this is a Flink Instant or a str, the dt will be set as is.
          (If you are creating this error event for use in a Flink pipeline, you'll
          want to pass in an Instant.)

    :param error_stream_name:
        Value to use for meta.stream.  This is optional, as some serializers
        (e.g. wikimedia-event-utilities JsonEventGenerator) handle setting meta.stream..

    :param errored_event:
        The input event that triggered this error.
        - If this is a dict, it will be serialized as a JSON string and set in the raw_event field.
        - Else if not None, it will be set as a str in the raw_event field.
    """

    stream_field = "meta.stream"
    schema_uri_field = "$schema"

    if isinstance(error_dt, datetime):
        error_dt = format_dt(error_dt)

    event: Dict[str, Any] = {
        "$schema": error_schema_uri,
        "meta": {
            "id": str(uuid.uuid4()),
            # NOTE: meta.dt should be set by tooling.
            "uri": pydash.objects.get(errored_event, "meta.uri", "unknown"),
            "domain": pydash.objects.get(errored_event, "meta.domain", "unknown"),
            "request_id": pydash.objects.get(errored_event, "meta.request_id", "unknown"),
        },
        "dt": error_dt,
        "emitter_id": emitter_id,
        "message": repr(error),
    }

    if error_stream_name:
        event["meta"]["stream"] = error_stream_name

    if isinstance(error, Exception):
        event["stack"] = "".join(TracebackException.from_exception(error).format())
        event["error_type"] = type(error).__name__

    if errored_event:
        # if errored_event a dict, we should be able to include
        # top level information in our error event about
        # which stream and which schema this validation
        # error is about.
        if isinstance(errored_event, dict):
            event["raw_event"] = json.dumps(errored_event, default=json_default_serializer)
            event["errored_schema_uri"] = pydash.objects.get(
                errored_event, schema_uri_field, "unknown"
            )
            event["errored_stream_name"] = pydash.objects.get(
                errored_event, stream_field, "unknown"
            )
        else:
            event["raw_event"] = str(errored_event)
            event["errored_schema_uri"] = "unknown"
            event["errored_stream_name"] = "unknown"

    return event
