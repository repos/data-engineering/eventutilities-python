"""
Python stream processing abstractions for use with Wikimedia Event Platform.
"""

from eventutilities_python.stream.manager import (
    stream_manager,
    load_config,
)

from eventutilities_python.stream.functions import (
    http_process_function,
)

from eventutilities_python.stream.descriptor import (
    EventStreamDescriptor,
    SourceDescriptor,
    SinkDescriptor,
)


# Export public API symbols
__all__ = [
    "stream_manager",
    "load_config",
    "http_process_function",
    "EventStreamDescriptor",
    "SourceDescriptor",
    "SinkDescriptor",
]
