"""
Function wrappers for the stream.manager.Stream process() `func`.
"""

from functools import partial, wraps
from typing import Optional, List, Callable, Any

import requests as requests

from requests.adapters import HTTPAdapter, Retry


def http_process_function(
    max_retries: int = 3,
    retry_backoff_factor: float = 0.1,
    retry_status_forcelist: Optional[List[int]] = None,
    pool_maxsize: int = 1,
) -> Callable[..., Any]:
    """Decorator that wraps a function and handles instantiation of a requests
    Session object, with a retry policy and a worker pool.

    Usage:
        >>> @http_process_function()
        >>> def my_func(event: dict, http_session: requests.Session):
        >>>     event["enriched"] = http_session.get(...)["enriched_data"]
        >>>     return event


    :param max_retries:
        Retry total retries

    :param retry_backoff_factor:
         Retry backoff_factor

    :param retry_status_forcelist:
        Retry status_forcelist. Default: [408, 500, 502, 503, 504]

    :param pool_maxsize:
        HTTPAdapter pool_maxsize
    """

    if retry_status_forcelist is None:
        retry_status_forcelist = [408, 500, 502, 503, 504]

    def decorator(func):
        @wraps(func)
        def wrapper():
            http_session = requests.Session()
            retries = Retry(
                total=max_retries,
                backoff_factor=retry_backoff_factor,
                status_forcelist=retry_status_forcelist,
            )
            adapter = HTTPAdapter(max_retries=retries, pool_maxsize=pool_maxsize)
            http_session.mount("http://", adapter)
            http_session.mount("https://", adapter)
            return partial(func, http_session=http_session)

        return wrapper()

    return decorator


# Export public API symbols
__all__ = [
    "http_process_function",
]
