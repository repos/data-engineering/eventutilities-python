import json
from pathlib import Path
from typing import List, Union

from eventutilities_python.stream.descriptor import SinkDescriptor
from eventutilities_python.type_aliases import EventDict


def read_records_from_flink_row_file_sink(sink: Union[str, SinkDescriptor]) -> List[EventDict]:
    """A helper function for reading JSON records out of a local output path
    that a Flink sink has written to. Useful for testing the results of a data
    pipeline after the Sink has written results.

    :param sink: Either a sink directory in which a Flink Row FileSink
        has written results, Or a file SinkDescriptor from which
        options.uri will be used as the sink.
    """
    if isinstance(sink, str):
        path = sink
    else:
        assert sink.connector == "file"
        path = sink.options["uri"]

    if path.startswith("file://"):
        path = path[7:]
    all_records = []

    for partition_dir in Path(path).iterdir():
        for data_file in partition_dir.iterdir():
            with open(data_file) as file:
                records = [json.loads(line) for line in file.readlines()]
                all_records += records

    return all_records


def assert_error_event(
    record,
    expected_error_message,
    expected_stream_name,
    expected_schema_uri,
    expected_error_type
):
    """Lil' helper to assert that an error event looks like it should."""
    assert record["message"] == expected_error_message, "error event should have error message"
    assert record["$schema"] == expected_schema_uri, "error event $schema should be set"
    assert "emitter_id" in record, "error event emitter_id should be set"
    assert "dt" in record, "error event dt should be set create_error_event"
    assert "raw_event" in record, "error event raw_event should be set"
    assert record["error_type"] == expected_error_type, "error event should have error_type"
    assert (
        record["meta"]["stream"] == expected_stream_name
    ), "error event meta.stream should be set by JsonEventGenerator"
    assert "dt" in record["meta"], "error event meta.dt should be set by JsonEventGenerator"
