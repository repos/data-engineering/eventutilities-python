import os
from typing import Any, Callable, Dict, TypeVar, Union, Sequence, Optional

import logging

# I'm not sure why we need to tell mypy to ignore this import.
# jsonargparse does indeed allow imports of these.
from jsonargparse.typing import register_type, path_type
from jsonargparse.util import is_absolute_path, change_to_path_dir
from pyflink.common import Row
import pathlib

CloseableIterator = TypeVar("CloseableIterator")
EventDict = Dict[str, Any]
EventDictToRowConverter = Callable[[EventDict], Row]
EventDictMapFunction = Callable[[EventDict], EventDict]
EventDictFilterFunction = Callable[[EventDict], bool]

# Registered with jsonargparse below.
UriAbsolute = TypeVar("UriAbsolute", bound=str)
"""UriAbsolute is just a type alias of str.

It exists to be able to allow jsonargparse to automatically deserialize
and resolve incoming URI parameters with this type.
"""

NestedStr = Sequence[Union[str, "NestedStr"]]
"""Recursive Sequence of strings.

Allows for [str], [str, [str, str]], etc.
"""

Path_uc = path_type("uc")
"""Jsonargparse Path URI that is either readable or potentially creatable.

https://jsonargparse.readthedocs.io/en/v4.20.0/#parsing-urls
"""


# TODO: it would be better to move this method to utils, but utils imports EventDict from
# here, causing a circular import.
def resolve_uris(
    uri: Union[str, NestedStr],
    relative_to: Optional[Sequence[str]] = None,
) -> Union[str, NestedStr]:
    """Resolves u as an absolute URI. Uses jsonargparse url parsing, as well as
    pathlib.Path.as_uri() to add file:// to the beginning of local file paths.

    :param uri: uri to resolve, or a (recursive) list of uris to resolve.
    :param relative_to: If not provided, will attempt to resolve only to
        cwd. If provided, each path here will be attempted to be used to
        resolve in order.  The first readable file found in one of these
        paths will be returned.
    :return: The resolved URI or (recursive) list of resolved URIs.
    """

    # If not a string, then uri is a list of uris, resolve them all.
    if not isinstance(uri, str):
        return [resolve_uris(u, relative_to) for u in uri]

    # A jsonargparse readable URI path type.
    # Can be used for auto parsing and resolving of URIs, both local and remote.
    # u == url, c == exists or createable.
    uri_readable = path_type("uc")

    if is_absolute_path(uri):
        logging.debug("No need to resolve %s, it is already absolute", uri)
        resolved_uri = uri_readable(uri)
    elif not relative_to:
        # If relative_to not provided, use uri_readable to resolve uri against cwd.
        logging.debug("Attempting to resolving URI %s against cwd %s", uri, os.getcwd())
        resolved_uri = uri_readable(uri)
    else:
        resolved_uri = None
        logging.debug("Attempting to resolve URI %s against base URIs %s", uri, relative_to)
        Path_c = path_type("c")
        while not resolved_uri:
            for relative_dir in relative_to:
                with change_to_path_dir(Path_c(relative_dir)):
                    try:
                        resolved_uri = uri_readable(uri)
                        break
                    except TypeError as e:
                        logging.debug(
                            "URI %s was not resolvable in %s, continuing search. ",
                            uri,
                            relative_dir,
                        )
                        continue
            # if we get here, we weren't able to resolve the uri.
            break

        if not resolved_uri:
            raise TypeError(f"Could not resolve readable URI {uri} relative to {relative_to}.")

    absolute_uri: str = resolved_uri.absolute
    # add file:// if absolute_uri is local and doesn't already have file://
    if absolute_uri.startswith("/"):
        absolute_uri = pathlib.Path(absolute_uri).as_uri()

    logging.debug("Resolved %s to %s", uri, absolute_uri)
    return absolute_uri


# Register UriAbsolute so it can be used with jsonargparse typing.
register_type(UriAbsolute, str, resolve_uris)
