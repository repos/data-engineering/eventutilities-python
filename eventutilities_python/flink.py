"""Helper methods and wrappers for working with PyFlink and Wikimedia's
eventutilities."""
from datetime import datetime

import importlib_resources

import json
import logging
import os
import tempfile
from collections import OrderedDict
from functools import partial
from pathlib import Path
from typing import Any, Collection, Dict, Optional, Sequence, Union

from py4j.java_gateway import JavaObject
from pyflink.common import Row, WatermarkStrategy, Instant
from pyflink.common.typeinfo import (
    MapTypeInfo,
    ObjectArrayTypeInfo,
    RowTypeInfo,
    TypeInformation,
    _from_java_type,
    InstantTypeInfo,
    TupleTypeInfo,
    BasicArrayTypeInfo,
    PrimitiveArrayTypeInfo,
)
from pyflink.datastream import StreamExecutionEnvironment, SourceFunction, SinkFunction
from pyflink.datastream.connectors import Sink, Source, DeliveryGuarantee
from pyflink.datastream.connectors.file_system import FileSource
from pyflink.datastream.connectors.kafka import (
    KafkaSink,
    KafkaSinkBuilder,
    KafkaSource,
    KafkaSourceBuilder,
    KafkaOffsetsInitializer,
)
from pyflink.datastream.data_stream import DataStream
from pyflink.java_gateway import get_gateway
from pyflink.util import java_utils

from eventutilities_python.type_aliases import EventDictToRowConverter
from eventutilities_python.utils import find_lib_jars, JavaObjectWrapper

default_kafka_producer_properties = {"max.request.size": "4194304"}
"""Used when building a Kafka sink to set some defaults."""


def flink_jvm():
    return get_gateway().jvm


def load_lib_dir(
    flink_env: StreamExecutionEnvironment,
    lib_dir: Optional[str] = None,
) -> None:
    """Add all jars in lib_dir to flink_env.

    If lib_dir is not defined, the value of the environment variable
    EVENTUTILITIES_LIB_DIR will be used. If EVENTUTILITIES_LIB_DIR is
    not set, jars are expected to be part of this package in the /lib
    directory.
    """
    if not lib_dir:
        lib_dir = os.environ.get("EVENTUTILITIES_LIB_DIR")
    if not lib_dir:
        lib_dir = str(importlib_resources.files(__package__).joinpath("lib"))

    # cast to str to avoid mypy warning.
    jars = find_lib_jars(str(lib_dir))

    logging.info(f"Adding jars from lib dir %s: %s ", lib_dir, ",".join(jars))

    flink_env.add_jars(*jars)


def get_flink_env(lib_dir: Optional[str] = None) -> StreamExecutionEnvironment:
    """Get a flink StreamExecutionEnvironment with lib_dir loaded."""
    env = StreamExecutionEnvironment.get_execution_environment()
    # Add all jars in lib_dir if it is defined.
    load_lib_dir(env, lib_dir)
    return env


def dict_to_properties(d: Dict[str, str]) -> JavaObject:
    """
    Converts a dict of string -> string key value pairs into java.util.Properties.
    """
    properties = flink_jvm().java.util.Properties()
    for k, v in d.items():
        properties.setProperty(k, str(v))
    return properties


def instant_to_datetime(instant: Instant) -> datetime:
    """
    Converts a Flink Instant type to a Python datetime.
    """
    ts_with_ms = instant.seconds + instant.nanos / 1000000000.0
    return datetime.fromtimestamp(ts_with_ms)


def datetime_to_instant(dt: datetime) -> Instant:
    """
    Converts a python datetime to a Flink Instant type.
    """
    return Instant(int(dt.timestamp()), dt.microsecond * 1000)


def convert_pyflink_to_python(value: Any) -> Any:
    """
    Converts PyFlink types to native python types.
    Most basic types have already been converted to python types
    by PyFlink.  However, there are a few types that eventutilities-python
    goes a little farther with:

    * Rows are converted to dicts.
    * Instants are converted to python datetimes.
    * Container types (array, tuple, dict and row) are recursed
      over to do the same conversions.

    Use convert_python_to_flink to reverse this operation.

    NOTE: We don't need the PyFlink TypeInformation here, because we can infer the
    desired Python type from the PyFlink type.  We convert Rows to dicts,
    and Maps are already converted to dicts by PyFlink.
    To do the reverse, the TypeInformation is needed, as we need to know
    what kind of PyFlink type a dict should be converted to.

    :param value:
        Value to fully convert to native python types.
    """
    if isinstance(value, Row):
        # NOTE: We don't need to use Row's recursive=True param
        # to recursively convert Rows to dicts, as we are
        # recursing through value's dict fields below already.
        value = value.as_dict(False)

    if isinstance(value, list):
        return [convert_pyflink_to_python(o) for o in value]
    elif isinstance(value, tuple):
        return tuple([convert_pyflink_to_python(o) for o in value])
    elif isinstance(value, dict):
        return dict((k, convert_pyflink_to_python(v)) for k, v in value.items())
    elif isinstance(value, Instant):
        return instant_to_datetime(value)
    else:
        return value


def convert_python_to_pyflink(value: Any, type_info: TypeInformation) -> Any:
    """
    Iterates over type_info and value and recursively converts python native
    types back to PyFlink types. This should be the reverse operation
    of convert_pyflink_to_python.

    Do not use this function to convert a PyFlink provided value that has not
    been previously converted to our python native types with convert_pyflink_to_python.

    :param value:
        Value to convert

    :param type_info:
        TypeInformation describing the PyFlink type that the value should be converted to.
    """
    if value is None:
        converted_value = None

    # Convert into Row
    elif isinstance(type_info, RowTypeInfo):
        # We must preserve insert order, so that we can have a mapping between
        # row names and their position. This will ensure that schemas can be compared,
        # a requirement for evolution and state management.
        converted_dict = OrderedDict()
        for field_name, field_type in zip(
            type_info.get_field_names(),
            type_info.get_field_types(),
        ):
            converted_dict[field_name] = convert_python_to_pyflink(
                value.get(field_name),
                field_type,
            )
        converted_value = Row(**converted_dict)

    # Convert all array elements.
    elif isinstance(
        type_info,
        (
            BasicArrayTypeInfo,
            PrimitiveArrayTypeInfo,
            ObjectArrayTypeInfo,
        ),
    ):
        converted_value = [
            convert_python_to_pyflink(element, type_info._element_type) for element in value
        ]

    # Convert all tuple elements.
    elif isinstance(type_info, TupleTypeInfo):
        converted_value = tuple(
            convert_python_to_pyflink(element, element_type_info)
            for element, element_type_info in zip(value, type_info.get_field_types())
        )
    # Convert all map elements.
    elif isinstance(type_info, MapTypeInfo):
        # Dict comprehension to recursively convert each key and value
        converted_value = {
            convert_python_to_pyflink(map_key, type_info._key_type_info): convert_python_to_pyflink(
                map_value,
                type_info._value_type_info,
            )
            for (map_key, map_value) in value.items()
        }

    # datetime -> Instant is the only basic (non-container) type that we convert (for now).
    elif isinstance(type_info, InstantTypeInfo) and isinstance(value, datetime):
        converted_value = datetime_to_instant(value)

    else:
        converted_value = value

    return converted_value


def python_to_pyflink_converter(type_info: TypeInformation) -> EventDictToRowConverter:
    """
    Returns a converter function for `type_info`.

    `python_to_pyflink_converter` delegates python type to flink type
    transformation to `convert_dicts_to_rows` by means of partial function application.

    An input dictionary will be passed to `convert_python_to_pyflink`
    as the call stack is built incrementally.

    `python_to_pyflink_converter` can be used in DataStream higher-order functions:
    Example
    >>> ds.map(python_to_pyflink_converter(type_info), type_info).map(...)
    """
    return partial(convert_python_to_pyflink, type_info=type_info)


class EventDataStreamFactory(JavaObjectWrapper):
    """A Python wrapper to eventutilities-flink's EventDataStreamFactory."""

    @classmethod
    def of(
        cls,
        schema_uris: Union[Sequence[str], str],
        stream_config_uri: str,
        http_client_routes: Optional[Dict[str, str]] = None,
    ) -> "EventDataStreamFactory":
        """:param schema_uris: Event schema repository URIs.  Event schemas
        will be loaded from these. Either a list or a csv.

        :param stream_config_uri:
            URI from which to get stream config.
            See org.wikimedia.eventutilities.core.event.EventStreamConfig

        :param http_client_routes:
            A mapping of source hostnames to dest hostnames.  If an HTTP request is to made
            to a source hostname, the request will be sent to dest hostname but with the
            Host header set to source hostname.  Used in WMF production environments
            where we need to use discovery MediaWiki API endpoints, e.g. api-ro.discovery.wmnet
        """

        j_EventDataStreamFactory = (
            flink_jvm().org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory
        )

        if isinstance(schema_uris, str):
            schema_uris = schema_uris.split(",")

        # TODO: rename "from" to "of" in java.
        j_EventDataStreamFactoryFrom = getattr(j_EventDataStreamFactory, "from")
        j_event_datastream_factory = j_EventDataStreamFactoryFrom(
            schema_uris, stream_config_uri, http_client_routes
        )
        return EventDataStreamFactory(j_event_datastream_factory)

    def get_event_stream_factory(self):
        """Returns the eventutilities-core Java EventStreamFactory instance.

        We may one day want to provide a python wrapper for this too.
        """
        return self.java_object.getEventStreamFactory()

    def stream_exists(self, stream_name: str) -> bool:
        """Returns true if the stream is declared in stream config."""
        return bool(
            self.get_event_stream_factory().getEventStreamConfig().streamExists(stream_name)
        )

    def get_row_type_info(
        self,
        stream_name: str,
        schema_version: str = "latest",
    ) -> TypeInformation:
        """Return the Flink RowTypeInfo for a given stream name and version.

        :param stream_name:
            Name of the stream

        :param schema_version:
            Version of the stream's event schema to use.

        """
        # j_type_info will be assigned an EventRowTypeInfo object.
        j_type_info: JavaObject = self.java_object.rowTypeInfo(stream_name, schema_version)
        return _from_java_type(j_type_info.toRowTypeInfo())

    def file_source(
        self,
        stream_name: str,
        uris: Collection[Union[str, Path]],
        schema_version: Optional[str] = None,
    ) -> FileSource:
        """Gets a FileSource that will deserialize NLJSON files using the
        stream_descriptor's schema.

        Note that there is no corresponding file_source_builder method,
        as there are not so many options to pass to the Java
        FileSourceBuilder, and PyFlink does not provide a
        FileSourceBuilder wrapper like it does for other SourceBuilders.

        :param stream_name:
            Name of the stream. Must be declared in event stream config.

        :param uris:
            String URIs or list of URIs to read NLJSON from.

        :param schema_version:
            Version of the stream's event schema to use.
            If None, latest will be used.
        """
        if isinstance(uris, str):
            uris = uris.split(",")

        j_Path = flink_jvm().org.apache.flink.core.fs.Path
        j_paths = java_utils.to_jarray(j_Path, [j_Path(uri) for uri in uris])

        j_file_source_builder = self.java_object.fileStreamSourceBuilder(
            stream_name,
            schema_version,
            j_paths,
        )
        j_file_source = j_file_source_builder.build()
        return FileSource(j_file_source)

    def collection_source(
        self,
        stream_name: str,
        source_data: Collection[Dict[str, Any]],
        schema_version: Optional[str] = None,
    ) -> Source:
        """Uses the provided source collection of dict to instantiate the input
        Source. Because PyFlink does not provide a way to use eventutilities-
        flink Java based JsonRowDeserializationSchema to instantiate a
        DataStream of Rows using the built in env.from_collection(), we mock a
        'collection source' by writing the collection out to a temporary file,
        and then using a FileSource to read it in.

        :param stream_name:
            Stream name.

        :param source_data: a list of dictionaries, each of them
                representing an event to deserialize.

        :param schema_version:
            Version of the stream's event schema to use.
            If None, latest will be used.

        """
        temp_file = tempfile.NamedTemporaryFile(delete=False, dir=tempfile.mkdtemp())
        # Temp data is fed to eventutilities-flink fileData, that expects a collection
        # of LineRecords. Append newline to ensure the temp file can be parsed.
        temp_file.writelines([bytes(json.dumps(d) + "\n", "utf-8") for d in source_data])

        return self.file_source(stream_name, [temp_file.name], schema_version)

    # Excluded from coverage (for now) as it is difficult to test, and
    # should only be used for development anyway.
    def sse_source(
        self,
        stream_name: str,
        uri: str = "https://stream.wikimedia.org/v2/stream",
        is_wmf_eventstreams_api: bool = True,
        schema_version: Optional[str] = None,
    ) -> SourceFunction:  # pragma: no cover
        """
        Gets a SourceFunction that reads JSON events from the SSE EventSource URI.
        This is useful for reading events from the WMF HTTP EventStreams API,
        e.g. https://stream.wikimedia.org/v2/stream.

        The default values of uri and is_wmf_eventstreams_api are enough to read
        a stream from the public stream.wikimedia.org endpoint.

        NOTE: This should not be used for 'production' jobs.
        It uses the deprecated Flink SourceFunction, and has not been thoroughly tested.
        This is mostly useful for development and testing.

        :param stream_name:
            Stream name.
            Must be declared in event stream config.

        :param uri:
            SSE EventSource URI endpoint from which to read JSON events.

        :param is_wmf_eventstreams_api:
            If True, the uri endpoint will be altered to use the stream name
            according the WMF EventStreams HTTP API.
            E.g. uri + /<stream_name>

        :param schema_version:
            Version of the stream's event schema to use.
            If None, latest will be used.

        """
        if is_wmf_eventstreams_api:
            uri = os.path.join(uri, stream_name)

        return SourceFunction(
            self.java_object.sseSourceFunction(
                stream_name,
                schema_version,
                uri,
            )
        )

    def kafka_source_builder(
        self,
        stream_name: str,
        bootstrap_servers: str,
        consumer_group: str,
        topics: Optional[Sequence[str]] = None,
        schema_version: Optional[str] = None,
    ) -> KafkaSourceBuilder:
        """Get a KafkaSourceBuilder that is primed with settings needed to
        consume the stream from Kafka.

        This sets the following:
        - bootstrapServers,
        - topics,
        - consumer group id
        - value only deserializer that will deserialize to a
          Row conforming to streamName's JSONSchema
        - starting offsets will use committed offsets,
          resetting to LATEST if no offsets are committed.

        If you want to change any of these settings, then call the appropriate
        method on the returned KafkaSourceBuilder before calling build().

        :param stream_name:
            Stream name.
            Must be declared in event stream config.

        :param bootstrap_servers:
            Kafka bootstrap.servers property.

        :param consumer_group:
            Kafka consumer.group.id property.

        :param topics:
            Topics to use. If None, the stream's configured topics will be used.

        :param schema_version:
            Version of the stream's event schema to use.
            If None, latest will be used.

        """

        j_source_builder = self.java_object.kafkaSourceBuilder(
            stream_name,
            schema_version,
            bootstrap_servers,
            consumer_group,
            topics,
        )

        source_builder = KafkaSourceBuilder()
        source_builder._j_builder = j_source_builder

        return source_builder

    def kafka_source(
        self,
        stream_name: str,
        bootstrap_servers: str,
        consumer_group: str,
        offsets_initializer: Optional[Union[KafkaOffsetsInitializer, int, str]] = None,
        properties: Optional[Dict[str, str]] = None,
        topics: Optional[Sequence[str]] = None,
        schema_version: Optional[str] = None,
    ) -> KafkaSource:
        """Returns a KafkaSource using kafka_source_builder, setting provided
        properties on the KafkaSourceBuilder.

        :param stream_name:
            Stream name.
            Must be declared in event stream config.

        :param bootstrap_servers:
            Kafka bootstrap.servers property.

        :param consumer_group:
            Kafka consumer.group.id property.

        :param offsets_initializer:
            EventDataStreamFactory kafkaSourceBuilder will set the default
            offset initializer to use committed offsets, or latest if none are found.

            Set offsets_initializer to override this.
            - If an int or a digit str, it is expected to be a timestamp
            - If a str, it should be one of "earliest" or "latest"
            - If a KafkaOffsetsInitializer, just use it.

            TODO: add support for specific offsets.

        :param properties:
            Additional properties to set on the KafkaSourceBuilder.

        :param topics:
            Topics to use. If None, the stream's configured topics will be used.

        :param schema_version:
            Version of the stream's event schema to use.
            If None, latest will be used.

        """
        source_builder = self.kafka_source_builder(
            stream_name=stream_name,
            schema_version=schema_version,
            bootstrap_servers=bootstrap_servers,
            consumer_group=consumer_group,
            topics=topics,
        )

        # If offsets_initializer, it should be a timestamp
        if isinstance(offsets_initializer, int) or (
            isinstance(offsets_initializer, str) and offsets_initializer.isdigit()
        ):
            offsets_initializer = KafkaOffsetsInitializer.timestamp(int(offsets_initializer))
        # else if a string, it should be either earliest or latest offsets
        elif isinstance(offsets_initializer, str):
            assert offsets_initializer in ["earliest", "latest"]
            offsets_initializer = getattr(KafkaOffsetsInitializer, offsets_initializer)()

        # TODO: provide ability to specify specific offsets and/or offset reset strategy.

        # If offsets_initializer is given, use it.
        # Note that java EventDataStreamFactory kafkaSourceBuilder
        # defaults to using committed offsets resetting from latest.
        if offsets_initializer:
            source_builder.set_starting_offsets(offsets_initializer)

        if properties:
            source_builder.set_properties(properties)

        return source_builder.build()

    def kafka_sink_builder(
        self,
        stream_name: str,
        schema_version: str,
        bootstrap_servers: str,
        topic: str,
    ) -> KafkaSinkBuilder:
        """Prepare a KafkaSinkBuilder with all the required components to
        produce to kafka using a json format matching the provided schema.

        The produced messages match the WMF Event Platform Rules:
        - the meta.dt field is filled and will be used as the kafka timestamp
        - the topic to produce to is one of the topics defined in the EventStream configuration
        - the provided schema must match the one defined in the EventStream configuration
        - the resulting json is validated against this schema
        - the dt (event time) field remains optional and must be set beforehand in the pipeline

        This sink builder will be configured to use a default random Kafka partitioner,
        and a timestamp strategy of KafkaRecordTimestampStrategy.ROW_EVENT_TIME.

        If you want to change any of these settings, then call the appropriate
        method on the returned KafkaSinkBuilder before calling build().

        TODO:
        - support custom kafka partitioner
        - support custom timestamp strategy

        :param stream_name:
            Stream name.
            Must be declared in event stream config.

        :param schema_version:
            Event schema version.  Must be an actual version. "latest" is not supported for Sinks.

        :param bootstrap_servers:
            Kafka bootstrap.servers property.

        :param topic:
            The Kafka topic to write to.
            If not one of the topics referenced by the EventStreamConfig, a warning will be logged.
        """

        timestamp_strategy = (
            flink_jvm().org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy.ROW_EVENT_TIME  # noqa: E501
        )

        j_sink_builder = self.java_object.kafkaSinkBuilder(
            stream_name,
            schema_version,
            bootstrap_servers,
            topic,
            timestamp_strategy,
        )

        sink_builder = KafkaSinkBuilder()
        sink_builder._j_builder = j_sink_builder
        return sink_builder

    def kafka_sink(
        self,
        stream_name: str,
        schema_version: str,
        bootstrap_servers: str,
        topic: str,
        delivery_guarantee: Optional[Union[DeliveryGuarantee, str]] = None,
        transactional_id_prefix: Optional[str] = None,
        properties: Optional[Dict[str, str]] = None,
    ) -> KafkaSink:
        """Instantiate new Kafka Sink for a streaming application.

        https://gerrit.wikimedia.org/r/c/wikimedia-event-utilities/+/868172/8/eventutilities/src/main/java/org/wikimedia/eventutilities/core/event/EventStream.java
        # noqa: E501

        :param stream_name:
            Stream Name.

        :param schema_version:
            Event schema version.  Must be an actual version. "latest" is not supported for Sinks.

        :param bootstrap_servers:
            Kafka bootstrap.servers property.

        :param topic:
            The Kafka topic to write to.
            If not one of the topics referenced by the EventStreamConfig,
            a warning will be logged.

        :param delivery_guarantee:
            Flink DeliveryGuarantee (or str name) to use for the Sink.
            https://nightlies.apache.org/flink/flink-docs-master/docs/connectors/datastream/kafka/#fault-tolerance

        :param transactional_id_prefix:
            Required if using DeliveryGuarantee.EXACTLY_ONCE.
            https://nightlies.apache.org/flink/flink-docs-master/docs/connectors/datastream/kafka/#fault-tolerance

        :param properties:
            Additional properties to set on the KafkaSinkBuilder.
        """

        if not properties:
            properties = {}

        # Apply global default kafka producer properties.
        properties = {**default_kafka_producer_properties, **properties}

        sink_builder = self.kafka_sink_builder(
            stream_name,
            schema_version,
            bootstrap_servers,
            topic,
        )

        if delivery_guarantee:
            if isinstance(delivery_guarantee, str):
                # Get the DeliveryGuarantee enum value from its string name.
                # Will raise KeyError if not supported name.
                delivery_guarantee = DeliveryGuarantee[delivery_guarantee]
            sink_builder.set_delivery_guarantee(delivery_guarantee)

        if delivery_guarantee == DeliveryGuarantee.EXACTLY_ONCE and transactional_id_prefix is None:
            raise ValueError(
                "When using kafka sink with DeliveryGuarantee.EXACTLY_ONCE, "
                "transactional_id_prefix is required but was not provided."
            )

        if transactional_id_prefix:
            sink_builder.set_transactional_id_prefix(transactional_id_prefix)

        # KafkaSinkBuilder does not have a set_properties (plural) method,
        # so iterate and call set_property on each.
        for k, v in properties.items():
            sink_builder.set_property(k, v)

        return sink_builder.build()

    def print_sink(
        self,
        stderr: bool = False,
    ) -> SinkFunction:  # pragma: no cover
        """
        A simple print sink.  No serializer will be used.
        Equivalent to using datastream.print().

        :param stderr:
            If true, the print sink will print to stderr instead of stdout

        NOTE: This should not be used for 'production' jobs.
        This is mostly useful for development and testing.
        """
        j_sink = flink_jvm().org.apache.flink.streaming.api.functions.sink.PrintSinkFunction(stderr)
        return SinkFunction(j_sink)

    def row_file_sink(
        self,
        stream_name: str,
        schema_version: str,
        uri: str,
    ) -> Sink:
        """Returns a FileSink that will write Rows as JSON strings with the
        default bucket assigner and rolling policies. Mostly useful for
        integration testing.

        :param stream_name:
            Stream Name.

        :param schema_version:
            Event schema version.  Must be an actual version. "latest" is not supported for Sinks.

        :param uri: Base uri location directory where partitioned output
                directories and files will be written.
        """
        j_sink = self.java_object.fileSinkBuilderRowFormat(
            stream_name,
            schema_version,
            uri,
        ).build()
        return Sink(j_sink)

    def from_source(
        self,
        env: StreamExecutionEnvironment,
        stream_name: str,
        source: Union[Source, SourceFunction],
        watermark_strategy: Optional[WatermarkStrategy] = None,
        source_name: Optional[str] = None,
        schema_version: str = "latest",
    ) -> DataStream:
        """Instantiates new DataStream from the provided Source using the
        RowTypeInfo of the stream_descriptor.

        :param env:
            an instance of Flink StreamExecutionEnvironment

        :param stream_name:
            Stream name

        :param schema_version:
            Event schema version

        :param source:
            Flink Source or SourceFunction.

        :param watermark_strategy:
            If none is provided, WatermarkStrategy.noWatermarks() will be used.
            If source is a SourceFunction, this will be ignored.

        :param source_name:
            Name of source, will be used in UI and metrics.
            If not provided, will use the stream name and version.
        """

        if not watermark_strategy:
            watermark_strategy = WatermarkStrategy.no_watermarks()

        if not source_name:
            source_name = f"source__{stream_name}:{schema_version}"

        source_type_info = self.get_row_type_info(stream_name, schema_version)

        if isinstance(source, Source):
            datastream = env.from_source(
                source=source,
                watermark_strategy=watermark_strategy,
                source_name=source_name,  # name the Source
                type_info=source_type_info,
            )
        elif isinstance(source, SourceFunction):
            datastream = env.add_source(
                source_func=source,
                source_name=source_name,
                type_info=source_type_info,
            )
        else:
            raise ValueError(
                f"source must be a Flink Source or SourceFunction, was {type(source)}."
            )

        return datastream
