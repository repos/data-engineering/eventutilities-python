"""Base tests for the stream_manager context"""
import json
import re

import mock
import pytest
from typing import Optional

from eventutilities_python.stream.error import error_schema_version
from eventutilities_python.stream.functions import http_process_function
from eventutilities_python.stream.manager import (
    stream_manager,
)
from eventutilities_python.stream.descriptor import SourceDescriptor, SinkDescriptor
from eventutilities_python.testing.utils import (
    read_records_from_flink_row_file_sink,
    assert_error_event,
)


def test_flink_stream_manager_init(
    fixture_schema_uris,
    fixture_stream_config_uri,
    source_descriptor_file_test_event_example,
    test_event_enriched,
):
    """
    Initialise a stream.
    It is expected that source and sink schemas are resolved, and source / sink are
    instantiated.
    """
    manager = stream_manager(
        job_name="test_manager",
        schema_uris=fixture_schema_uris,
        stream_config_uri=fixture_stream_config_uri,
        source=source_descriptor_file_test_event_example,
        sink=SinkDescriptor(
            connector="kafka",
            stream=str(test_event_enriched),
            options={
                "bootstrap_servers": "localhost:9092",
            },
        ),
    )
    assert isinstance(manager.source_descriptor, SourceDescriptor)
    assert isinstance(manager.sink_descriptor, SinkDescriptor)
    assert manager.sink_descriptor.stream_descriptor == test_event_enriched
    assert manager.error_sink_descriptor is None


def test_flink_stream_manager_init_with_default_custom_sink(
    fixture_schema_uris,
    fixture_stream_config_uri,
    source_descriptor_file_test_event_example,
    test_event_enriched,
    test_event_error,
):
    manager = stream_manager(
        job_name="test_manager",
        schema_uris=fixture_schema_uris,
        stream_config_uri=fixture_stream_config_uri,
        source=source_descriptor_file_test_event_example,
        sink=SinkDescriptor(
            connector="kafka",
            stream=str(test_event_enriched),
            options={
                "bootstrap_servers": "localhost:9092",
            },
        ),
        error_sink=SinkDescriptor(connector="kafka", stream=str(test_event_error)),
    )
    assert isinstance(manager.error_sink_descriptor, SinkDescriptor)
    assert manager.error_sink_descriptor.stream_descriptor == test_event_error


def test_stream_unknown(
    fixture_schema_uris,
    fixture_stream_config_uri,
    source_descriptor_file_test_event_example,
    test_event_enriched,
):
    """
    When consuming or producing a stream that does not exist,
    eventutilities will raise a java.io.FileNotFoundException (local schema repo).
    In Python it will be wrapped by a Py4JJavaError.
    TODO: we could wrap the error handling in
    EventDataStreamFactory to return python Exceptions.
    """
    source_exist = source_descriptor_file_test_event_example

    source_not_exist = SourceDescriptor(
        connector="file",
        stream="test.not.existing:1.1.0",
        options=source_exist.options,
    )

    source_latest = SourceDescriptor(
        connector="file",
        stream=f"{source_exist.stream_descriptor.name}:latest",
        options=source_exist.options,
    )

    dest_exist = SinkDescriptor(
        connector="kafka",
        stream=str(test_event_enriched),
        options={
            "bootstrap_servers": "localhost:9092",
        },
    )

    dest_not_exist = SinkDescriptor(
        connector="kafka",
        stream=str("test.not.existing:1.1.0"),
        options={
            "bootstrap_servers": "localhost:9092",
        },
    )

    dest_latest = SinkDescriptor(
        connector="kafka",
        stream=f"{test_event_enriched.name}:latest",
        options={
            "bootstrap_servers": "localhost:9092",
        },
    )

    with pytest.raises(ValueError):
        with stream_manager(
            job_name="test_manager",
            schema_uris=fixture_schema_uris,
            stream_config_uri=fixture_stream_config_uri,
            source=source_not_exist,
            sink=dest_exist,
        ) as stream:
            pass

    with pytest.raises(ValueError):
        with stream_manager(
            job_name="test_manager",
            schema_uris=fixture_schema_uris,
            stream_config_uri=fixture_stream_config_uri,
            source=source_exist,
            sink=dest_not_exist,
        ) as stream:
            # We have to call execute to get the sink sink to be instantiated
            stream.execute()

    # 'latest' version tag is not allowed
    # A source stream is always required
    with pytest.raises(ValueError):
        with stream_manager(
            job_name="test_manager",
            schema_uris=fixture_schema_uris,
            stream_config_uri=fixture_stream_config_uri,
            source=source_latest,
            sink=dest_exist,
        ) as stream:
            pass

    with pytest.raises(ValueError):
        with stream_manager(
            job_name="test_manager",
            schema_uris=fixture_schema_uris,
            stream_config_uri=fixture_stream_config_uri,
            source=source_exist,
            sink=dest_latest,
        ) as stream:
            pass


def test_flink_stream_manager_enrich(
    tmp_path,
    flink_env,
    fixture_schema_uris,
    fixture_stream_config_uri,
    source_descriptor_file_test_event_example,
    test_event_enriched,
):
    """
    Test that a FlinkStreamManager enrichment pipeline works:
    Row -> dict, enrich, then dict -> Row.
    """

    def enrich(test_event: dict) -> dict:
        assert isinstance(test_event, dict)
        test_event["enriched_field"] = "I am enriched"
        return test_event

    main_output_path = tmp_path / "main_output"

    with stream_manager(
        job_name="test_manager",
        schema_uris=fixture_schema_uris,
        stream_config_uri=fixture_stream_config_uri,
        source=source_descriptor_file_test_event_example,
        sink=SinkDescriptor(
            connector="file",
            stream=str(test_event_enriched),
            options={"uri": "file://" + str(main_output_path)},
        ),
        process_max_workers_default=2,
        process_batch_size_default=1,
    ) as stream:
        stream.partition_by(lambda e: e["test_decimal"])
        stream.process(enrich)
        stream.execute()

    records = read_records_from_flink_row_file_sink(str(main_output_path))

    assert len(records) == 4
    for record in records:
        assert (
            record["enriched_field"] == "I am enriched"
        ), "enriched_field should be set by enrich fn"
        assert (
            record["$schema"] == f"/test/event_enriched/{test_event_enriched.version}"
        ), "$schema should be set by stream manager"
        # meta.stream and meta.dt should be set by JsonEventGenerator
        assert (
            record["meta"]["stream"] == test_event_enriched.name
        ), "meta.stream should be set by JsonEventGenerator"
        assert "dt" in record["meta"], "meta.dt should be set by JsonEventGenerator"


def test_flink_stream_manager_enrich_with_error(
    tmp_path,
    flink_env,
    fixture_schema_uris,
    fixture_stream_config_uri,
    flink_datastream_factory,
    source_descriptor_file_test_event_example,
    test_event_enriched,
    test_event_error,
):
    def enrich_with_error(test_event: dict) -> dict:
        assert isinstance(test_event, dict)
        if test_event["test_int"] == 2:
            raise ValueError("GOT AN ERROR HERE")

        test_event["enriched_field"] = "I am enriched"
        return test_event

    main_output_path = tmp_path / "main_output"
    error_output_path = tmp_path / "error_output"

    with stream_manager(
        job_name="test_manager",
        schema_uris=fixture_schema_uris,
        stream_config_uri=fixture_stream_config_uri,
        source=source_descriptor_file_test_event_example,
        sink=SinkDescriptor(
            connector="file",
            stream=str(test_event_enriched),
            options={"uri": f"file://{str(main_output_path)}"},
        ),
        error_sink=SinkDescriptor(
            connector="file",
            stream=str(test_event_error),
            options={"uri": f"file://{str(error_output_path)}"},
        ),
        process_max_workers_default=2,
        process_batch_size_default=1,
    ) as stream:
        stream.partition_by(lambda e: e["test_decimal"])
        stream.process(enrich_with_error)
        stream.execute()

    records = read_records_from_flink_row_file_sink(str(main_output_path))
    assert len(records) == 3
    for record in records:
        assert (
            record["enriched_field"] == "I am enriched"
        ), "enriched_field should be set by enrich fn"
        assert (
            record["$schema"] == f"/test/event_enriched/{test_event_enriched.version}"
        ), "$schema should be set by stream manager"
        # meta.stream and meta.dt should be set by JsonEventGenerator
        assert (
            record["meta"]["stream"] == test_event_enriched.name
        ), "error event meta.stream should be set by JsonEventGenerator"
        assert "dt" in record["meta"], "error event meta.dt should be set by JsonEventGenerator"

    error_records = read_records_from_flink_row_file_sink(str(error_output_path))
    assert len(error_records) == 1
    for record in error_records:
        exception = ValueError("GOT AN ERROR HERE")
        assert_error_event(
            record,
            repr(exception),
            test_event_error.name,
            f"/error/{test_event_error.version}",
            type(exception).__name__
        )


def test_flink_stream_manager_enrich_multiple_process_and_errors(
    stream_with_error_sink,
    test_event_error,
):
    test_int_to_enriched_fields = {
        1: "I am enriched by enrich1",
        2: "I am enriched by enrich2",
    }

    test_int_to_error_message = {
        3: repr(ValueError("error in enrich1")),
        4: repr(ValueError("error in enrich2")),
    }

    def enrich1(test_event: dict) -> dict:
        if test_event["test_int"] == 3:
            raise ValueError("error in enrich1")

        test_event["enriched_field"] = "I am enriched by enrich1"
        return test_event

    def enrich2(enriched_event: dict) -> dict:
        if enriched_event["test_int"] == 4:
            raise ValueError("error in enrich2")

        elif enriched_event["test_int"] == 2:
            enriched_event["enriched_field"] = "I am enriched by enrich2"
        return enriched_event

    with stream_with_error_sink as stream:
        stream.partition_by(lambda e: e["test_decimal"])
        stream.process(enrich1)
        # need to key by again before calling process again.
        stream.partition_by(lambda e: e["test_decimal"])
        stream.process(enrich2)
        stream.execute()

    main_output_uri = stream_with_error_sink.sink_descriptor.options["uri"]
    main_output_path = re.sub("^file://", "", main_output_uri)

    error_output_uri = stream_with_error_sink.error_sink_descriptor.options["uri"]
    error_output_path = re.sub("^file://", "", error_output_uri)

    records = read_records_from_flink_row_file_sink(main_output_path)
    assert len(records) == 2
    for record in records:
        if "enriched_field" in record:
            assert (
                record["enriched_field"] == test_int_to_enriched_fields[record["test_int"]]
            ), f"enriched_field should be set by an enrich function"

    error_records = read_records_from_flink_row_file_sink(error_output_path)
    assert len(error_records) == 2
    for record in error_records:
        triggering_event = json.loads(record["raw_event"])
        message = test_int_to_error_message[triggering_event["test_int"]]
        assert_error_event(
            record,
            message,
            test_event_error.name,
            f"/error/{test_event_error.version}",
            type(ValueError(message)).__name__  # test_int_to_error_message throws ValueError
        )


def test_flink_stream_manager_enrich_invalid_output(
    stream_with_error_sink,
    test_event_error,
):
    def enrich(test_event: dict) -> Optional[dict]:
        if test_event["test_int"] != 3:
            return test_event

    with stream_with_error_sink as stream:
        stream.partition_by(lambda e: e["test_decimal"])
        stream.process(enrich)
        stream.execute()

    error_output_uri = stream_with_error_sink.error_sink_descriptor.options["uri"]
    error_output_path = re.sub("^file://", "", error_output_uri)

    error_records = read_records_from_flink_row_file_sink(error_output_path)
    assert len(error_records) == 1
    for record in error_records:
        exception = ValueError("'None': invalid EventProcessFunction output record.")
        assert_error_event(
            record,
            repr(exception),
            test_event_error.name,
            f"/error/{test_event_error.version}",
            type(exception).__name__
        )


def test_async_http_process_function_decorator(
    stream_with_error_sink,
    test_event_error,
):
    @http_process_function()
    def http_enrichment(event, http_session):
        if event["test_int"] == 3:
            raise ValueError("error in Enrich process function")
        with mock.patch.object(http_session, "get") as get_mock:
            # This code is execute on a beam worker thread,
            # so we need to patch the instance of requests
            # instantiated in that thread.
            get_mock.return_value = mock_response = mock.Mock()
            mock_response.status_code = 200
            mock_response.text = "content body"
            response = http_session.get("https://some-endpont.tld")
            event["enriched_field"] = response.text
            return event

    with stream_with_error_sink as stream:
        stream.partition_by(lambda e: e["test_decimal"])
        stream.process(http_enrichment)
        stream.execute()

    main_output_uri = stream_with_error_sink.sink_descriptor.options["uri"]
    main_output_path = re.sub("^file://", "", main_output_uri)

    error_output_uri = stream_with_error_sink.error_sink_descriptor.options["uri"]
    error_output_path = re.sub("^file://", "", error_output_uri)

    records = read_records_from_flink_row_file_sink(main_output_path)
    assert len(records) == 3
    for record in records:
        if "enriched_field" in record:
            assert (
                record["enriched_field"] == "content body"
            ), f"enriched_field should be set by an enrich function"

    error_records = read_records_from_flink_row_file_sink(error_output_path)
    assert len(error_records) == 1
    for record in error_records:
        exception = ValueError("error in Enrich process function")
        assert_error_event(
            record,
            repr(exception),
            test_event_error.name,
            f"/error/{test_event_error.version}",
            type(exception).__name__
        )
