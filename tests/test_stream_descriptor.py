import pytest

from eventutilities_python.stream.descriptor import EventStreamDescriptor


def test_stream_descriptor_schema_default_version():
    stream_descriptor = EventStreamDescriptor(name="test.event.stream")
    assert stream_descriptor.version == "latest"


def test_stream_descriptor_parsing():
    stream_descriptor = EventStreamDescriptor.from_string("test.event.stream:1.1.0")
    assert stream_descriptor.name == "test.event.stream"
    assert stream_descriptor.version == "1.1.0"

    stream_descriptor = EventStreamDescriptor.from_string("test.event.stream:latest")
    assert stream_descriptor.name == "test.event.stream"
    assert stream_descriptor.version == "latest"


def test_invalid_stream_descriptor():
    with pytest.raises(ValueError):
        EventStreamDescriptor.from_string(None)

    # Schema version is required.
    with pytest.raises(ValueError):
        EventStreamDescriptor.from_string("test.event.stream")

    # Invalid input. Name is missing.
    with pytest.raises(ValueError):
        EventStreamDescriptor.from_string(":1.1.0")
