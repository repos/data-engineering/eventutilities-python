"""
A test suite for data deserialization from files and collections of dictionaries.
"""
from datetime import datetime

from pyflink.common import Instant

from eventutilities_python.flink import convert_pyflink_to_python, python_to_pyflink_converter
from eventutilities_python.testing.utils import read_records_from_flink_row_file_sink

test_event_example_expected = [
    {
        "$schema": "/test/event/1.1.0",
        "dt": datetime.fromtimestamp(1546300800.000),
        "meta": {"stream": "test.event.example", "dt": None, "id": None},
        "test": "expected value",
        "test_int": 1,
        "test_decimal": 2.0,
        "test_map": {"key1": "val1", "key2": "val2"},
        "test_array": None,
    },
    {
        "$schema": "/test/event/1.1.0",
        "dt": datetime.fromtimestamp(1546300800.000),
        "meta": {"stream": "test.event.example", "dt": None, "id": None},
        "test": "other value",
        "test_int": 2,
        "test_decimal": 2.0,
        "test_map": {"key1": "val1", "key2": "val2"},
        "test_array": None,
    },
    {
        "$schema": "/test/event/1.1.0",
        "dt": datetime.fromtimestamp(1546300800.000),
        "meta": {"stream": "test.event.example", "dt": None, "id": None},
        "test": "expected value",
        "test_int": 3,
        "test_decimal": 2.0,
        "test_map": {"key1": "val1", "key2": "val2"},
        "test_array": None,
    },
    {
        "$schema": "/test/event/1.1.0",
        "dt": datetime.fromtimestamp(1546300800.000),
        "meta": {"stream": "test.event.example", "dt": None, "id": None},
        "test": "other value",
        "test_int": 4,
        "test_decimal": 2.0,
        "test_map": {"key1": "val1", "key2": "val2"},
        "test_array": None,
    },
]


def execute_and_compare(datastream, expected):
    results = list(datastream.execute_and_collect())
    assert len(results) == len(expected)
    assert all(results)
    assert all(list(x in results for x in expected))


def test_from_files(
    flink_env,
    flink_datastream_factory,
    test_event_example_files,
    test_event_example,
):
    """
    Test data event data can be loaded and deserialised from files.
    """
    source = flink_datastream_factory.file_source(
        stream_name=test_event_example.name,
        uris=test_event_example_files,
    )
    datastream = flink_datastream_factory.from_source(
        env=flink_env,
        stream_name=test_event_example.name,
        schema_version=test_event_example.version,
        source=source,
    ).map(
        convert_pyflink_to_python
    )  # convert to dict for ease of comparison
    execute_and_compare(datastream, test_event_example_expected)


def test_from_data(
    flink_env,
    flink_datastream_factory,
    test_event_example_data,
    test_event_example,
):
    """
    Test that event data can be loaded and deserialised from a collection of dictionaries.
    """
    source = flink_datastream_factory.collection_source(
        stream_name=test_event_example.name,
        source_data=test_event_example_data,
    )
    datastream = flink_datastream_factory.from_source(
        env=flink_env,
        stream_name=test_event_example.name,
        schema_version=test_event_example.version,
        source=source,
    ).map(
        convert_pyflink_to_python
    )  # convert to dict for ease of comparison
    execute_and_compare(datastream, test_event_example_expected)


def test_row_file_sink(
    tmp_path,
    flink_env,
    flink_datastream_factory,
    test_event_example_files,
    test_event_example,
):
    """
    Test that a datastream can be serialized through the eventutilities-flink
    JsonRowSerializationSchema -> JsonEventGenerator by writing
    the data into a row record file sink.
    """
    source = flink_datastream_factory.file_source(
        stream_name=test_event_example.name,
        uris=test_event_example_files,
    )
    datastream = flink_datastream_factory.from_source(
        env=flink_env,
        stream_name=test_event_example.name,
        schema_version=test_event_example.version,
        source=source,
    ).map(
        convert_pyflink_to_python
    )  # convert to dict for ease of comparison

    def transform(e):
        e["test"] = "transformed by test"
        # Delete meta.dt and meta.stream to be sure it
        # is set by the JsonEventGenerator serialization.
        del e["meta"]["dt"]
        del e["meta"]["stream"]
        return e

    datastream = datastream.map(transform)

    # convert back to Rows.
    output_type_info = flink_datastream_factory.get_row_type_info(
        test_event_example.name,
        test_event_example.version,
    )
    row_converter = python_to_pyflink_converter(output_type_info)
    datastream = datastream.map(row_converter, output_type_info)

    file_sink = flink_datastream_factory.row_file_sink(
        stream_name=test_event_example.name,
        schema_version=test_event_example.version,
        uri="file://" + str(tmp_path),
    )

    datastream.sink_to(file_sink)
    datastream.get_execution_environment().execute()

    result_records = read_records_from_flink_row_file_sink(str(tmp_path))
    for record in result_records:
        assert record["test"] == "transformed by test"
        assert record["$schema"] == f"/test/event/{test_event_example.version}"
        # meta.stream and meta.dt should be set by JsonEventGenerator
        assert record["meta"]["stream"] == test_event_example.name
        assert record["meta"]["dt"]
