"""Tests for creating stream_manager from config"""
import json
import yaml
from jsonargparse import ArgumentParser

from eventutilities_python.stream.manager import (
    stream_manager,
    load_config,
)
from eventutilities_python.stream.descriptor import SourceDescriptor, SinkDescriptor


def test_stream_manager_from_args_base(
    fixture_stream_manager_argv,
    test_event_enriched,
):
    config = load_config(argv=fixture_stream_manager_argv)
    manager = stream_manager.from_config(config)

    assert isinstance(manager.source_descriptor, SourceDescriptor)
    assert isinstance(manager.sink_descriptor, SinkDescriptor)
    assert manager.sink_descriptor.stream_descriptor == test_event_enriched
    assert manager.error_sink_descriptor is None


def test_stream_manager_from_args_custom_arg(
    fixture_stream_manager_argv,
    test_event_enriched,
):
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--custom0", type=str)

    argv = fixture_stream_manager_argv + ["--custom0", "value0"]

    config = load_config(argv=argv, parser=arg_parser)
    assert config.custom0 == "value0"

    manager = stream_manager.from_config(config)
    assert isinstance(manager.source_descriptor, SourceDescriptor)
    assert isinstance(manager.sink_descriptor, SinkDescriptor)
    assert manager.sink_descriptor.stream_descriptor == test_event_enriched
    assert manager.error_sink_descriptor is None


# @pytest.mark.skip(reason="This does not work! " "See comment on error_sink param in stream_manage")
def test_stream_manager_from_args_with_error_sink_configured(
    fixture_stream_manager_argv, test_event_enriched, test_event_error
):
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--custom0", type=str)

    argv = fixture_stream_manager_argv + [
        "--custom0",
        "value0",
        "--stream_manager.error_sink.connector",
        "file",
        "--stream_manager.error_sink.stream",
        str(test_event_error),
        "--stream_manager.error_sink.options",
        json.dumps({"uri": "/tmp/errors"}),
        # "--stream_manager.error_sink.class_path",
        # "eventutilities_python.manager.ErrpSinkDescriptor",
    ]
    #
    # defaults = {
    #     "--stream_manager.error_sink.connector": "file",
    #     "--stream_manager.error_sink.stream": str(test_event_error),
    #     "--stream_manager.error_sink.options": {"uri": "/tmp/errors"},
    # }
    config = load_config(argv=argv, parser=arg_parser)
    assert config.custom0 == "value0"

    manager = stream_manager.from_config(config)
    assert isinstance(manager.source_descriptor, SourceDescriptor)
    assert isinstance(manager.sink_descriptor, SinkDescriptor)
    assert manager.sink_descriptor.stream_descriptor == test_event_enriched
    assert manager.error_sink_descriptor
    assert manager.error_sink_descriptor.connector == "file"
    assert manager.error_sink_descriptor.stream == str(test_event_error)


def test_stream_manager_from_args_with_config_file(
    fixture_stream_manager_config_file,
    test_event_enriched,
):
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--custom0", type=str)

    argv = [
        "--custom0",
        "value1",
        "--config",
        str(fixture_stream_manager_config_file),
    ]

    config = load_config(argv=argv, parser=arg_parser)
    assert config.custom0 == "value1"

    manager = stream_manager.from_config(config)
    assert isinstance(manager.source_descriptor, SourceDescriptor)
    assert isinstance(manager.sink_descriptor, SinkDescriptor)
    assert manager.sink_descriptor.stream_descriptor == test_event_enriched
    assert manager.error_sink_descriptor is None


def test_stream_manager_from_args_with_multiple_config_sources_and_relative_uri(
    tmp_path, fixture_stream_manager_config_file, test_event_enriched, test_event_error
):
    arg_parser = ArgumentParser()
    arg_parser.add_argument("--custom0", type=str)

    config2_file_path = tmp_path / "config2.yaml"

    config2_config = {
        "custom0": "override_value2",
        stream_manager.argparse_namespace: {
            "http_client_routes": {"http://example.org": "http://example.net"},
            "schema_uris": "http://schema.wikimedia.org/secondary/jsonschema",
            "source": {"options": {"uris": ["./tmp_test_event.json"]}},
            "sink": {
                "connector": "kafka",
                "options": {
                    "bootstrap_servers": "localhost:9092",
                    "kafka_topic_prefix": "codfw.",
                },
            },
            # Setting this to a string and sink.connector
            # to kafka will enable auto error sink configuration.
            "error_sink": {
                "connector": "kafka",
                "stream": str(test_event_error),
                "options": {
                    "bootstrap_servers": "localhost:9092",
                    "kafka_topic_prefix": "codfw.",
                },
            },
        },
    }

    with open(str(config2_file_path), "w") as config2_file:
        yaml.dump(
            config2_config,
            config2_file,
            default_flow_style=False,
        )

    argv = [
        "--config",
        str(fixture_stream_manager_config_file),
        "--custom0",
        "orig value",
        "--config",
        str(config2_file_path),
        # Override topic on CLI
        "--stream_manager.sink.options.topic",
        "override.test.event.enriched",
        "--stream_manager.error_sink.options.topic",
        "override.error_sink.topic",
    ]

    config = load_config(argv=argv, parser=arg_parser)
    assert (
        config.custom0 == "override_value2"
    ), "--custom0 should be overridden by second config file"

    manager = stream_manager.from_config(config)
    assert manager.schema_uris == [
        "http://schema.wikimedia.org/secondary/jsonschema"
    ], "--schema_uris should be overridden by second config file"

    expected_source_uris = [f"file://{str(tmp_path / 'tmp_test_event.json')}"]
    assert manager.source_descriptor.options["uris"] == expected_source_uris, (
        "source.options.uris should be overidden by and resolved as URI relative "
        "to second config file."
    )

    assert manager.sink_descriptor.options == {
        "bootstrap_servers": "localhost:9092",
        "topic": "override.test.event.enriched",
        # topic is being manually overridden,so kafka_topic_prefix will be ignored
        "kafka_topic_prefix": "codfw.",
    }, "sink options should be merged from multiple sources"

    assert (
        manager.error_sink_descriptor.connector == "kafka"
    ), "error_sink should be configured from config file"

    assert manager.error_sink_descriptor.options == {
        "bootstrap_servers": "localhost:9092",
        "topic": "override.error_sink.topic",
        # topic is being manually overridden,so kafka_topic_prefix will be ignored
        "kafka_topic_prefix": "codfw.",
    }, "error sink options should be merged from multiple sources"
