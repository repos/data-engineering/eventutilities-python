import time
from collections import namedtuple
from datetime import datetime, timezone

import py4j
import pyflink.datastream
import pytest
from py4j.java_gateway import JavaObject
from pyflink.common import Instant, Row
from pyflink.common.typeinfo import Types, _from_java_type
from pyflink.datastream import StreamExecutionEnvironment

from eventutilities_python.flink import (
    EventDataStreamFactory,
    convert_python_to_pyflink,
    python_to_pyflink_converter,
    flink_jvm,
    instant_to_datetime,
    datetime_to_instant,
    convert_pyflink_to_python,
)

DictWithTypeInfo = namedtuple("DictWithTypeInfo", ["record_dict", "type_info"])


fixture_dt = datetime(2023, 11, 1, 1, 20, 30, 123456)
# Construct an equivalent Instant with datetime timestamp (seconds) and nanoseconds.
# datetime does not have nanosecond precision, so we multiply the microsecond by 1000.
fixture_instant = Instant(int(fixture_dt.timestamp()), fixture_dt.microsecond * 1000)


@pytest.fixture(scope="session")
def dict_record_fixture() -> DictWithTypeInfo:
    type_info = Types.ROW_NAMED(
        [
            "str_field",
            "nested",
            "array_str_field",
            "array_dt_field",
            "other",
            "array_obj_field",
            "tuple_field",
        ],
        [
            Types.STRING(),
            Types.ROW_NAMED(["f1", "dt"], [Types.STRING(), Types.INSTANT()]),
            Types.BASIC_ARRAY(Types.STRING()),
            Types.BASIC_ARRAY(Types.INSTANT()),
            Types.STRING(),
            Types.OBJECT_ARRAY(
                Types.MAP(
                    # Key type
                    Types.STRING(),
                    # Complex value type
                    Types.ROW_NAMED(["str_field", "int_field"], [Types.STRING(), Types.BIG_INT()]),
                )
            ),
            Types.TUPLE([Types.INSTANT(), Types.ROW_NAMED(["s1"], [Types.STRING()])]),
        ],
    )

    record_dict = {
        "str_field": "str value",
        "nested": {"f1": "f1 value", "dt": fixture_dt},
        "array_str_field": ["v1", "v2"],
        "array_dt_field": [fixture_dt],
        "other": None,
        "array_obj_field": [
            {"key1": {"str_field": "v1", "int_field": 1111}},
            {"key2": {"str_field": "v2", "int_field": None}},
        ],
        "tuple_field": (fixture_dt, {"s1": "sv1"}),
    }
    return DictWithTypeInfo(record_dict, type_info)


@pytest.fixture(scope="session")
def row_fixture() -> Row:
    return Row(
        **{
            "str_field": "str value",
            "nested": Row(**{"f1": "f1 value", "dt": fixture_instant}),
            "array_str_field": ["v1", "v2"],
            "array_dt_field": [fixture_instant],
            "other": None,
            "array_obj_field": [
                {"key1": Row(**{"str_field": "v1", "int_field": 1111})},
                {"key2": Row(**{"str_field": "v2", "int_field": None})},
            ],
            "tuple_field": (fixture_instant, Row(**{"s1": "sv1"})),
        }
    )


def is_event_row_type_info(j_type_info: JavaObject) -> bool:
    """
    Check whether a Java TypeInfo object is an
    org.wikimedia.eventutilities.flink.EventRowTypeInfo.

    :param j_type_info:any Java TypeInfo object.
    :return:
    """
    JEventRowTypeInfo = flink_jvm().org.wikimedia.eventutilities.flink.EventRowTypeInfo
    # pyflink.common.typeinfo._is_instance_of checks for
    # assignability. Since JEventRowTypeInfo is a subclass of JRowTypeInfo,
    # here we need to check for strict equality
    return j_type_info.getClass().equals(JEventRowTypeInfo._java_lang_class)


def test_instant_to_datetime():
    converted_dt = instant_to_datetime(fixture_instant)
    assert converted_dt == fixture_dt


def test_datetime_to_instant():
    converted_instant = datetime_to_instant(fixture_dt)
    assert converted_instant == fixture_instant


def test_convert_pyflink_to_python(row_fixture, dict_record_fixture):
    result = convert_pyflink_to_python(row_fixture)

    assert result == dict_record_fixture.record_dict


def test_convert_python_to_pyflink(dict_record_fixture, row_fixture):
    """
    Test conversion from a Python dict to a Flink Row.
    """
    record_row = convert_python_to_pyflink(
        dict_record_fixture.record_dict, dict_record_fixture.type_info
    )

    assert record_row == row_fixture


def test_pyflink_python_conversion_roundtrip(row_fixture, dict_record_fixture):
    row = convert_python_to_pyflink(
        convert_pyflink_to_python(row_fixture), dict_record_fixture.type_info
    )
    assert row == row_fixture


def test_dict_datastream_to_row(dict_record_fixture, row_fixture):
    """
    Test conversion from a DataStream of dictionaries to a DataStream
    of Rows.
    """
    env = StreamExecutionEnvironment.get_execution_environment()
    datastream = (
        env.from_collection(
            [dict_record_fixture.record_dict]
        )  # delegate type casting to dict_datastream_to_row
        .map(python_to_pyflink_converter(type_info=dict_record_fixture.type_info))
        .map(lambda row: row == row_fixture)
    )

    assert isinstance(datastream, pyflink.datastream.DataStream)
    assert all(datastream.execute_and_collect("test_dict_datastream_to_row"))


def test_http_client_routes(fixture_schema_uris, fixture_stream_config_uri):
    http_client_routes = {}
    datastream_factory = EventDataStreamFactory.of(
        fixture_schema_uris.split(","), fixture_stream_config_uri, http_client_routes
    )
    assert datastream_factory

    # Test that an EventStreamFactory can be instantiated with non Null http_client_routes.
    # We should check that the mapping is properly set,
    # but config properties are internal to the Builder.
    # An alternative would be mocking the request and validate that schema init hits
    # the configured (routed) URI. This would require wrapping WireMock
    # (or equivalent client/server utils), since the HTTP requests
    # are performed by the JVM and not a Python library.
    http_client_routes = {fixture_stream_config_uri: "api-discovery-ro.mock"}
    # In the test suite we load config uri from a file, so the request won't be routed
    datastream_factory = EventDataStreamFactory.of(
        fixture_schema_uris.split(","), fixture_stream_config_uri, http_client_routes
    )

    assert datastream_factory


def test_invalid_http_client_routes(fixture_schema_uris):
    # If we try to route to meta via a non-existing URI, an exception will be raised.
    # [...]
    # Request to uri
    # http://some.config.uri.mock/w/api.php?format=json&action=streamconfigs&all_settings=true
    # failed.
    #   BasicHttpResult(failure)  encountered local exception:
    #       api-disocvery-ro.mock: nodename nor servname provided, or not known
    # [...]
    #
    with pytest.raises(py4j.protocol.Py4JJavaError) as exc_info:
        meta_stream_config_uri = "http://some.config.uri.mock/w/api.php"
        http_client_routes = {meta_stream_config_uri: "api-discovery-ro.mock"}
        EventDataStreamFactory.of(
            fixture_schema_uris.split(","), meta_stream_config_uri, http_client_routes
        )
    # Check that Py4JJavaError was trigger by an invalid route, and not by some other
    # issue int the factory builder chain. It uses an heuristic to match error message variations
    # across different software versions.
    assert exc_info.match("(?i)api-discovery-ro.mock: .* not known")


def test_event_row_type_info_conversion():
    JEventRowTypeInfo = flink_jvm().org.wikimedia.eventutilities.flink.EventRowTypeInfo

    nested_row_type_info = Types.ROW([Types.INT(), Types.STRING()])
    row_type_info = Types.ROW([nested_row_type_info])

    # We should be able to handle nested EventRowTypeInfo objects
    event_row_type_info = JEventRowTypeInfo.create(row_type_info.get_java_type_info())
    assert is_event_row_type_info(event_row_type_info)
    for j_type_info in event_row_type_info.getFieldTypes():
        assert is_event_row_type_info(j_type_info)

    assert is_event_row_type_info(event_row_type_info)
    assert _from_java_type(event_row_type_info.toRowTypeInfo())


def test_kafka_sink_delivery_guarantee(flink_env, flink_datastream_factory, test_event_example):
    with pytest.raises(ValueError):
        flink_datastream_factory.kafka_sink(
            stream_name=test_event_example.name,
            schema_version=test_event_example.version,
            bootstrap_servers="test-broker1001.domain.org:9092",
            topic="test_topic",
            delivery_guarantee="EXACTLY_ONCE",
            transactional_id_prefix=None,
            properties=None,
        )


# Check that a keyed stream configuration is properly initialized in the
# wrapped java code. eventutilities_python does not access key information directly,
# but under the hood it will be propagated to KafkaSink (in the wrapped Java code path).
# This test is minimal, and has been introduced only to ensure the right mediawiki-event-utilities
# jar is bundled. Unit tests have been implemented in the Java suite.
def test_keyed_stream_config(
    fixture_schema_uris,
    fixture_stream_config_uri,
    test_event_example_keyed
):
    datastream_factory = EventDataStreamFactory.of(fixture_schema_uris.split(","), fixture_stream_config_uri, None)

    # Extract a Java EventRowTypeInfo instance.
    event_row_type_info = datastream_factory.java_object.rowTypeInfo(test_event_example_keyed.name, test_event_example_keyed.version)
    expected_key_type_info = Types.ROW_NAMED(["key_alias"], [Types.STRING()])

    assert event_row_type_info.hasKey() == True
    assert _from_java_type(event_row_type_info.keyTypeInfo().toRowTypeInfo()) == expected_key_type_info

    j_row = event_row_type_info.createEmptyRow()
    j_row.setField("test_map", {"key1": "abc"})
    j_row.setField("test_int", 123)

    j_row_expected = event_row_type_info.keyTypeInfo().createEmptyRow()
    j_row_expected.setField("key_alias", "abc")

    assert event_row_type_info.extractKey(j_row).equals(j_row_expected)
    # The RecordSerializer instance is private. Guarding for regression
    # against behaviour changes should be covered in integration tests.
