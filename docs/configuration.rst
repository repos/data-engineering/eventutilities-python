Configuration
*************

``stream_manager`` is configurable via the following ``yaml`` settings.
All options can be overwritten at cli. See ``python your_app.py -h`` for more details.

.. literalinclude :: assets/config.yaml
   :language: yaml
