.. Eventutilities Python documentation master file, created by
   sphinx-quickstart on Wed Jun  7 11:59:09 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Eventutilities Python's documentation!
=================================================

.. toctree::
   :maxdepth: 2

   README

Design Document
^^^^^^^^^^^^^^^
.. toctree::
   :maxdepth: 2

   DESIGN

Tutorial
^^^^^^^^
.. toctree::
   :maxdepth: 2

   TUTORIAL

Config
^^^^^^
.. toctree::
   :maxdepth: 2

   configuration

API
^^^
.. toctree::
   :maxdepth: 2

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
