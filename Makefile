# Do not modify PROJECT_VERSION manually. Use bump2version instead.
PROJECT_VERSION := 0.19.0.dev

# A docker image based atop debian buster. It provides the same run-time as Gitlab CI.
DOCKER_IMG := platform/pyflink
DOCKERFILE := Dockerfile
# Generate x86_64 binaries when running docker on non x86 platform (e.g. apple m1, m2).
DOCKER_PLATFORM := linux/amd64

# Location of eventutilities-python Java dependencies.
# install runtime deps into eventutilities_python/lib so they
# will be included in the packaged python .wheel.
EVENTUTILITIES_LIB_DIR := $(shell pwd)/eventutilities_python/lib
FLINK_VERSION := 1.20.0
FLINK_CONNECTOR_KAFKA_VERSION := 3.3.0-1.20
EVENTUTILITIES_VERSION := 1.4.0
KAFKA_VERSION := 3.4.0

# Sphinx documentation path
DOCS_DIR := docs

# Build an x86_64-linux docker image.
pyflink-image:
	docker buildx build --platform ${DOCKER_PLATFORM} -t ${DOCKER_IMG} -f ${DOCKERFILE} .

ifeq ($(USE_DOCKER),true)
CURRENT_DIR := $(shell pwd)
DOCKER_CMD := docker run -it \
			  --rm \
			  --platform ${DOCKER_PLATFORM} \
			  -v ${CURRENT_DIR}:/root \
			  -e SKIP_DOCKER=true \
			  -w /root ${DOCKER_IMG}

# The docker image is required to run
# CI targets in a docker container.
test: pyflink-image
lint: pyflink-image
runtime_deps: pyflink-image
endif

# Download runtime Java dependencies. These are needed to run tests.
install_runtime_deps:
	if [ ! -d ${EVENTUTILITIES_LIB_DIR} ]; then \
		mkdir -p ${EVENTUTILITIES_LIB_DIR} && \
		curl -o ${EVENTUTILITIES_LIB_DIR}/eventutilities-flink-${EVENTUTILITIES_VERSION}-jar-with-dependencies.jar https://archiva.wikimedia.org/repository/releases/org/wikimedia/eventutilities-flink/${EVENTUTILITIES_VERSION}/eventutilities-flink-${EVENTUTILITIES_VERSION}-jar-with-dependencies.jar && \
		curl -o ${EVENTUTILITIES_LIB_DIR}/kafka-clients-${KAFKA_VERSION}.jar https://archiva.wikimedia.org/repository/mirror-maven-central/org/apache/kafka/kafka-clients/${KAFKA_VERSION}/kafka-clients-${KAFKA_VERSION}.jar && \
		curl -o ${EVENTUTILITIES_LIB_DIR}/flink-connector-kafka-${FLINK_CONNECTOR_KAFKA_VERSION}.jar https://archiva.wikimedia.org/repository/mirror-maven-central/org/apache/flink/flink-connector-kafka/${FLINK_CONNECTOR_KAFKA_VERSION}/flink-connector-kafka-${FLINK_CONNECTOR_KAFKA_VERSION}.jar && \
		curl -o ${EVENTUTILITIES_LIB_DIR}/flink-python-${FLINK_VERSION}-tests.jar https://archiva.wikimedia.org/repository/mirror-maven-central/org/apache/flink/flink-python/${FLINK_VERSION}/flink-python-${FLINK_VERSION}-tests.jar; \
	fi
# Run the pytest suite.
test:	install_runtime_deps
	${DOCKER_CMD} bash -c "pip3 install .[test] && \
	EVENTUTILITIES_LIB_DIR=${EVENTUTILITIES_LIB_DIR} tox -e test"

# Run linting with flake8
lint:
	${DOCKER_CMD} bash -c "tox -e lint"

# Run type-checking with mypy
type-check:
	${DOCKER_RUN} bash -c "tox -e type-check"

# Generate documentation for `stream_manager` config file.
# `sphinx-argparse` and similar extensions seem to
# conflict with our implementation of `jsonargparse` and/or the `autodoc`
# extension. As a workaround, a `config.yaml` asset is generated at (doc)
# build time and embedded in `docs/configuration.rst`.
generate_doc_assets:
	test -d ${DOCS_DIR}/assets/ || mkdir -p ${DOCS_DIR}/assets/
	python3 eventutilities_python/stream/manager.py --print_config=comments > ${DOCS_DIR}/assets/config.yaml

clean:
	rm -rf ./eventutilities_python/lib ./eventutilities_python/build/
	$(MAKE) -C ${DOCS_DIR} clean

# We don't specify a dep on `generate_doc_assets`, but wrap it's invocation
# ${DOCKER_CMD} instead. This ensures that assets are generated both when running
# the make target natively as well as in Docker.
html:
	${DOCKER_CMD} bash -c "pip3 install .[docs] \
		&& sphinx-apidoc -o ${DOCS_DIR} eventutilities_python/ \
		&& $(MAKE) generate_doc_assets \
		&& $(MAKE) -C ${DOCS_DIR} html"

.PHONY: pyflink-image test lint type-check install_runtime_deps generate_doc_assets html
