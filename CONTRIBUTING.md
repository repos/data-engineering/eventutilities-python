# How to contribute

This project follows contribution guidelines for [Event Platform Value Stream](https://www.mediawiki.org/wiki/Platform_Engineering_Team/Event_Platform_Value_Stream/Contribution).

Install test/dev deps with:
```
pip install tox
pip install -e .[test]
```

## Dependencies

Building this project requires either conda or Docker installed. To run `make` target in a Docker 
container export `USE_DOCKER=true`.

### Before submitting

Before submitting your code please do the following steps:

1. Add any changes you want
1. Add tests for the new changes
1. Edit documentation if you have changed something significant
1. Run `make test` to run unittests.
1. Run `make lint` to ensure that types, security and docstrings are okay.
